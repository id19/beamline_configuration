from bliss.setup_globals import *

# define tomo object used for mvct, mvrct umvct, umvrct, cts functions    
#init_tomo(hrtomo)

# define tomo object used for image correction object
#image_corr.set_tomo(hrtomo)

load_script("jibril.py")


print("")
print("Welcome to your new 'jibril' BLISS session !! ")
print("")
print("You can now customize your 'jibril' session by changing files:")
print("   * /jibril_setup.py ")
print("   * /jibril.yml ")
print("   * /scripts/jibril.py ")
print("")

