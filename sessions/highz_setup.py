
from bliss.setup_globals import *

load_script("highz.py")

print("")
print("Welcome to your new 'highz' BLISS session !! ")
print("")
print("You can now customize your 'highz' session by changing files:")
print("   * /highz_setup.py ")
print("   * /highz.yml ")
print("   * /scripts/highz.py ")
print("")