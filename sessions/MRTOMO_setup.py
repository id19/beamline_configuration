import inspect
from bliss.setup_globals import *
from id19.motorcalibration import *
from tomo.presets import DefaultChainImageCorrOnOffPreset, ImageCorrOnOffPreset
from tomo.presets import ReferenceMotorPreset, DefaultChainFastShutterPreset
from tomo.presets import DemoShutterPreset, FastShutterPreset, DarkShutterPreset, DetectorSafetyPreset
from tomo.utils import setup
from tomo.standard import *
from tomo.beamline.ID19.tools import *
from tomo.beamline.ID19.presets import OpiomPreset, DefaultChainOpiomPreset, PcoTomoOpiomPreset
from tomo.beamline.ID19.sequencepresets import FastShutterSequencePreset, MusstMaxAccuracySequencePreset, RemoveBackwardDispPreset
from tomo import globals as tomo_globals

# VV 2024-06-10: For testing
tomo_globals.USE_METADATA_FOR_SCAN = True

def restore_default_chain(lima_detector):
    """Restore the default chain configuration"""
    if lima_detector is None:
        return
    det_mode = SimpleSetting(f'mode_{lima_detector.name}', default_value='SINGLE')
    det_trig = SimpleSetting(f'trig_{lima_detector.name}', default_value='INTERNAL_TRIGGER')
    if det_mode.get() == 'ACCUMULATION':
        lima_detector.acquisition.mode = 'ACCUMULATION'
        if det_trig.get() == 'EXTERNAL_TRIGGER_MULTI':
            DEFAULT_CHAIN.set_settings(MRTOMO_ext_chain_acc["chain_config"])
        if det_trig.get() == 'INTERNAL_TRIGGER':
            DEFAULT_CHAIN.set_settings(MRTOMO_int_chain_acc["chain_config"])
    else:
        lima_detector.acquisition.mode = 'SINGLE'
        if det_trig.get() == 'EXTERNAL_TRIGGER_MULTI':
            DEFAULT_CHAIN.set_settings(MRTOMO_ext_chain["chain_config"])
        if det_trig.get() == 'INTERNAL_TRIGGER':
            DEFAULT_CHAIN.set_settings(MRTOMO_int_chain["chain_config"])


# If true, don't use the shutter for the scans
MDT = False

if MDT:
    print("========================================================")
    print("=    Be careful MDT=True; shutters will not be used    =")
    print("========================================================")


tomo_config = mrtomo_config
tomo_config.set_active()

full_tomo = mrfull_tomo
multi_tomo = mrmulti_tomo
pco_tomo = mrpco_tomo
z_series = mrz_series
try:
   z_series.init_sequence()
except:
   pass

detectors_optic = mrdetectors_optic
align_tomo = mralign_tomo

active_detector = get_controller_lima()

try:
    restore_default_chain(active_detector)
except:
    pass

if not MDT:
    fastshutter_preset = DefaultChainFastShutterPreset(exp_shutter)
    DEFAULT_CHAIN.add_preset(fastshutter_preset)

#opiompreset = DefaultChainOpiomPreset(multiplexer_tomo, detectors_optic=mrdetectors_optic, tomo_station='mr')
#DEFAULT_CHAIN.add_preset(opiompreset)

imagecorr_preset = DefaultChainImageCorrOnOffPreset(image_corr)
DEFAULT_CHAIN.add_preset(imagecorr_preset)


# Open Flint by default
try:
    SCAN_DISPLAY.auto=True
except NameError:
    # From Daiquiri Scan_DISPLAY is not defined, and we dont want Daiquiri to launch Flint
    pass


# initialise machine information
try:
    machinfo.initialize()
except:
    print("\nNo machine information available!\n")


print("")
print("Welcome to your new 'MRTOMO' BLISS session !! ")
print("")
print("You can now customize your 'MRTOMO' session by changing files:")
print("   * /MRTOMO_setup.py ")
print("   * /MRTOMO.yml ")
print("   * /scripts/MRTOMO.py ")
print("")


referencemotorpreset = ReferenceMotorPreset(mrref_displ)
#referencemotorpreset = ReferenceMotorPreset(medref_displ)
fastshutterpreset = FastShutterPreset(exp_shutter)
darkshutterpreset = DarkShutterPreset(bsh2)
imagecorrpreset = ImageCorrOnOffPreset()

flat_scan=full_tomo.get_runner('flat')
flat_scan.add_scan_preset(referencemotorpreset,"motor_preset")
dark_scan=full_tomo.get_runner('dark')

tomo_config.add_scan_preset(['flat', 'dark', 'count'], imagecorrpreset)
if not MDT:
    tomo_config.add_scan_preset(['flat', 'return_ref', 'CONTINUOUS', 'HELICAL','tiling'], fastshutterpreset)
    #tomo_config.add_scan_preset(['dark'], darkshutterpreset)
    tomo_config.add_scan_preset(['count'], fastshutterpreset)

musstsequencepreset = MusstMaxAccuracySequencePreset(musst)

full_tomo.add_sequence_preset(updateonoff, 'updateonoff')
full_tomo.add_sequence_preset(imagecorronoff, 'imagecorronoff')
full_tomo.add_sequence_preset(mrhalfacquipreset, 'halfacquipreset')
full_tomo.add_sequence_preset(musstsequencepreset, 'musstsequencepreset')

if not MDT:
    fastshuttersequencepreset = FastShutterSequencePreset(z_series, exp_shutter)
    z_series.add_sequence_preset(fastshuttersequencepreset, 'fastshuttersequencepreset')

z_series.add_sequence_preset(updateonoff, 'updateonoff')
z_series.add_sequence_preset(imagecorronoff, 'imagecorronoff')
z_series.add_sequence_preset(mrhalfacquipreset, 'halfacquipreset')
z_series.add_sequence_preset(musstsequencepreset, 'musstsequencepreset')

opiompreset = OpiomPreset(multiplexer_tomo, detectors_optic=mrdetectors_optic, tomo_station='mr')
fscan_mrtomo.add_scan_preset(opiompreset,"opiompreset")

_pcotomoopiompreset = PcoTomoOpiomPreset(multiplexer_tomo)
tomo_config.add_scan_preset(["MULTITURNS"],_pcotomoopiompreset)

if not MDT:
    dimaxshutterpreset = DetectorSafetyPreset(exp_shutter)

fscan = fscan_mrtomo.get_runner('fscan')
fscan.pars.motor = srot
fsweep = fscan_mrtomo.get_runner('fsweep')
fsweep.pars.motor = srot
finterlaced = fscan_mrtomo.get_runner('finterlaced')
finterlaced.pars.motor = srot
fscan2d = fscan_mrtomo.get_runner('fscan2d')
if not MDT:
    fscan2d.add_scan_preset(dimaxshutterpreset, "dimaxshutterpreset")
f2scan = fscan_mrtomo.get_runner('f2scan')
ftimescan = fscan_mrtomo.get_runner('ftimescan')
ftimescan.add_scan_preset(fastshutterpreset, "fastshutterpreset")
fscanloop = fscan_mrtomo.get_runner('fscanloop')
fscanloop.pars.motor = srot

accpreset = RemoveBackwardDispPreset(pco_tomo)
pco_tomo.add_sequence_preset(accpreset, 'removebackwarddisppreset')
accpreset = RemoveBackwardDispPreset(multi_tomo)
multi_tomo.add_sequence_preset(accpreset, 'removebackwarddisppreset')

def clemence_pulse(width=100,delay=10):
    #width and delay in ms
    opiom2_tpg = get_config().get("opiom2_tpg")
    opiom2_tpg.opiom.comm_ack("CNT 2 RESET")
    cmd = "CNT 2 CLK8 RISE D32 PULSE "+str(int(width*8000))+" "+str(int(delay*8000))+" "+str(1)
    opiom2_tpg.opiom.comm_ack(cmd)
    opiom2_tpg.opiom.comm_ack("CNT 2 START")

def inform_user():
    print("\nyou can send trigger now")

pco_tomo._inttrigger_func = clemence_pulse
pco_tomo._exttrigger_func = inform_user
    
fscanloop.set_lima_ready_trigger_func(inform_user)

# define tomo object used for image correction object
image_corr.set_tomo(full_tomo)

load_script("MRTOMO.py")
load_script("utils.py")
load_script("tomo.py")




init_tomo(full_tomo)


