
from bliss.setup_globals import *

load_script("berru.py")

print("")
print("Welcome to your new 'berru' BLISS session !! ")
print("")
print("You can now customize your 'berru' session by changing files:")
print("   * /berru_setup.py ")
print("   * /berru.yml ")
print("   * /scripts/berru.py ")
print("")