
from bliss.setup_globals import *

load_script("MEDTOMO.py")

print("")
print("Welcome to your new 'MEDTOMO' BLISS session !! ")
print("")
print("You can now customize your 'MEDTOMO' session by changing files:")
print("   * /MEDTOMO_setup.py ")
print("   * /MEDTOMO.yml ")
print("   * /scripts/MEDTOMO.py ")
print("")