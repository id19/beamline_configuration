from bliss.setup_globals import *
from bliss.common import measurementgroup
from tomo.Presets import DefaultChainFastShutterPreset,DefaultChainImageCorrOnOffPreset

# set the default chain configuration
DEFAULT_CHAIN.set_settings(mrtomo.tomo_ccd.def_chain_single_mode['chain_config'])

# add shutter control to default scans
shutter_preset = DefaultChainFastShutterPreset(mrtomo.shutter)

DEFAULT_CHAIN.add_preset(shutter_preset)
imagecorr_preset = DefaultChainImageCorrOnOffPreset()
DEFAULT_CHAIN.add_preset(imagecorr_preset)

# Open Flint by default
SCAN_DISPLAY.auto=True

# initialise machine information
try:
    machinfo.initialize()
except:
    print("\nNo machine information available!\n")

# define alignment
from tomo.beamline.ID19.alignment import *

load_script("PCOTOMO.py")

from id19.Tools import *
# define tomo object used for mvct, mvrct umvct, umvrct, cts functions    
init_tomo(mrtomo)

#apply the correct steps_per_unit to the current fucus motor
mrtomo.optic.focus_config_apply()

# define tomo object used for image correction object
image_corr.set_tomo(mrtomo)

print("")
print("Welcome to your new 'PCOTOMO' BLISS session !! ")
print("")
print("You can now customize your 'PCOTOMO' session by changing files:")
print("   * /PCOTOMO_setup.py ")
print("   * /PCOTOMO.yml ")
print("   * /scripts/PCOTOMO.py ")
print("")

