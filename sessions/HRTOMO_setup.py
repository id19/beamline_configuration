import inspect
from bliss.setup_globals import *
from id19.motorcalibration import *
from tomo.presets import DefaultChainImageCorrOnOffPreset, ImageCorrOnOffPreset
from tomo.presets import ReferenceMotorPreset, DefaultChainFastShutterPreset
from tomo.presets import DemoShutterPreset, FastShutterPreset, DarkShutterPreset, DetectorSafetyPreset
from tomo.utils import setup
from tomo.standard import *
from tomo.beamline.ID19.tools import *
from tomo.beamline.ID19.presets import OpiomPreset, DefaultChainOpiomPreset, PcoTomoOpiomPreset
from tomo.beamline.ID19.sequencepresets import FastShutterSequencePreset, MusstMaxAccuracySequencePreset, RemoveBackwardDispPreset
from tomo import globals as tomo_globals

# VV 2024-06-10: For testing
tomo_globals.USE_METADATA_FOR_SCAN = True

def restore_default_chain(lima_detector):
    """Restore the default chain configuration"""
    if lima_detector is None:
        return
    det_mode = SimpleSetting(f'mode_{lima_detector.name}', default_value='SINGLE')
    det_trig = SimpleSetting(f'trig_{lima_detector.name}', default_value='INTERNAL_TRIGGER')
    if det_mode.get() == 'ACCUMULATION':
        lima_detector.acquisition.mode = 'ACCUMULATION'
        if det_trig.get() == 'EXTERNAL_TRIGGER_MULTI':
            DEFAULT_CHAIN.set_settings(HRTOMO_ext_chain_acc["chain_config"])
        if det_trig.get() == 'INTERNAL_TRIGGER':
            DEFAULT_CHAIN.set_settings(HRTOMO_int_chain_acc["chain_config"])
    else:
        lima_detector.acquisition.mode = 'SINGLE'
        if det_trig.get() == 'EXTERNAL_TRIGGER_MULTI':
            DEFAULT_CHAIN.set_settings(HRTOMO_ext_chain["chain_config"])
        if det_trig.get() == 'INTERNAL_TRIGGER':
            DEFAULT_CHAIN.set_settings(HRTOMO_int_chain["chain_config"])

tomo_config = hrtomo_config
tomo_config.set_active()

full_tomo = hrfull_tomo
multi_tomo = hrmulti_tomo
pco_tomo = hrpco_tomo
z_series = hrz_series
try:
    z_series.init_sequence()
except:
    pass


detectors_optic = hrdetectors_optic
align_tomo = hralign_tomo


active_detector = full_tomo.get_controller_lima()

try:
    restore_default_chain(active_detector)
except:
    pass


#fastshutter_preset = DefaultChainFastShutterPreset(exp_shutter)
#DEFAULT_CHAIN.add_preset(fastshutter_preset)

#opiompreset = DefaultChainOpiomPreset(multiplexer_tomo, detectors_optic=hrdetectors_optic, tomo_station='hr')
#DEFAULT_CHAIN.add_preset(opiompreset)

imagecorr_preset = DefaultChainImageCorrOnOffPreset(image_corr)
DEFAULT_CHAIN.add_preset(imagecorr_preset)

# Open Flint by default
try:
    SCAN_DISPLAY.auto=True
except NameError:
    # From Daiquiri Scan_DISPLAY is not defined, and we dont want Daiquiri to launch Flint
    pass


# initialise machine information
try:
    machinfo.initialize()
except:
    print("\nNo machine information available!\n")


print("")
print("Welcome to your new 'HRTOMO' BLISS session !! ")
print("")
print("You can now customize your 'HRTOMO' session by changing files:")
print("   * /HRTOMO_setup.py ")
print("   * /HRTOMO.yml ")
print("   * /scripts/HRTOMO.py ")
print("")


referencemotorpreset = ReferenceMotorPreset(hrref_displ)
#referencemotorpreset = ReferenceMotorPreset(tomopressref_displ)
fastshutterpreset = FastShutterPreset(exp_shutter)
darkshutterpreset = DarkShutterPreset(bsh2)
imagecorrpreset = ImageCorrOnOffPreset()

flat_scan=full_tomo.get_runner('flat')
flat_scan.add_scan_preset(referencemotorpreset,"motor_preset")
dark_scan=full_tomo.get_runner('dark')

tomo_config.add_scan_preset(['flat', 'dark', 'count'], imagecorrpreset)
#tomo_config.add_scan_preset(['flat', 'return_ref', 'CONTINUOUS', 'tiling'], fastshutterpreset)
#tomo_config.add_scan_preset(['dark'], darkshutterpreset)
#tomo_config.add_scan_preset(['count'], fastshutterpreset)

musstsequencepreset = MusstMaxAccuracySequencePreset(musst)

full_tomo.add_sequence_preset(updateonoff, 'updateonoff')
full_tomo.add_sequence_preset(imagecorronoff, 'imagecorronoff')
full_tomo.add_sequence_preset(hrhalfacquipreset, 'halfacquipreset')
full_tomo.add_sequence_preset(musstsequencepreset, 'musstsequencepreset')

#fastshuttersequencepreset = FastShutterSequencePreset(z_series, exp_shutter)
z_series.add_sequence_preset(updateonoff, 'updateonoff')
#z_series.add_sequence_preset(fastshuttersequencepreset, 'fastshuttersequencepreset')
z_series.add_sequence_preset(imagecorronoff, 'imagecorronoff')
z_series.add_sequence_preset(hrhalfacquipreset, 'hrhalfacquipreset')
z_series.add_sequence_preset(musstsequencepreset, 'musstsequencepreset')

opiompreset = OpiomPreset(multiplexer_tomo, detectors_optic=hrdetectors_optic, tomo_station='hr')
fscan_hrtomo.add_scan_preset(opiompreset,"opiompreset")

_pcotomoopiompreset = PcoTomoOpiomPreset(multiplexer_tomo)
tomo_config.add_scan_preset(["MULTITURNS"],_pcotomoopiompreset)

fscan = fscan_hrtomo.get_runner('fscan')
fscan.pars.motor = srot
fsweep = fscan_hrtomo.get_runner('fsweep')
fsweep.pars.motor = srot
finterlaced = fscan_hrtomo.get_runner('finterlaced')
finterlaced.pars.motor = srot
fscan2d = fscan_hrtomo.get_runner('fscan2d')
f2scan = fscan_hrtomo.get_runner('f2scan')


dimaxshutterpreset = DetectorSafetyPreset(exp_shutter)

ftimescan = fscan_hrtomo.get_runner('ftimescan')
ftimescan.add_scan_preset(dimaxshutterpreset,"dimaxshutterpreset")
fscanloop = fscan_hrtomo.get_runner('fscanloop')
fscanloop.pars.motor = srot

accpreset = RemoveBackwardDispPreset(pco_tomo)
pco_tomo.add_sequence_preset(accpreset, 'removebackwarddisppreset')

def clemence_pulse(width=100,delay=10):
    #width and delay in ms
    opiom2_tpg = get_config().get("opiom2_tpg")
    opiom2_tpg.opiom.comm_ack("CNT 2 RESET")
    cmd = "CNT 2 CLK8 RISE D32 PULSE "+str(int(width*8000))+" "+str(int(delay*8000))+" "+str(1)
    opiom2_tpg.opiom.comm_ack(cmd)
    opiom2_tpg.opiom.comm_ack("CNT 2 START")

def inform_user():
    print("\nyou can send trigger now")
    
pco_tomo._inttrigger_func = clemence_pulse
pco_tomo._exttrigger_func = inform_user
    
fscanloop.set_lima_ready_trigger_func(inform_user)

# define tomo object used for image correction object
image_corr.set_tomo(full_tomo)

load_script("HRTOMO.py")
load_script("utils.py")
load_script("tomo.py")


init_tomo(full_tomo)
