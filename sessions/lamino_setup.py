from bliss.setup_globals import *
from bliss.common import measurementgroup
from id19.presets.FastShutterChainPreset import FastShutterChainPreset
from id19.ImageCorrection import ImageCorrection
#test
# set the default chain configuration
DEFAULT_CHAIN.set_settings(default_acq_chain['chain_config'])

# add shutter control to default scans
shutter_preset = FastShutterChainPreset(exp_shutter)
DEFAULT_CHAIN.add_preset(shutter_preset)

# visual image correction
image_corr = ImageCorrection(pcolid19det1, exp_shutter, 0.1)

# Open Flint by default
#SCAN_DISPLAY.auto=True

# initialise machine information
try:
    machinfo.initialize()
except:
    print("\nNo machine information available!\n")  

load_script("lamino.py")

print("")
print("Welcome to your new 'lamino' BLISS session !! ")
print("")
print("You can now customize your 'lamino' session by changing files:")
print("   * /lamino_setup.py ")
print("   * /lamino.yml ")
print("   * /scripts/lamino.py ")
print("")
