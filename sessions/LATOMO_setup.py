import inspect
from bliss.setup_globals import *
from id19.motorcalibration import *
from tomo.presets import DefaultChainImageCorrOnOffPreset, ImageCorrOnOffPreset
from tomo.presets import ReferenceMotorPreset, DefaultChainFastShutterPreset
from tomo.presets import DemoShutterPreset, FastShutterPreset, DarkShutterPreset
from tomo.utils import setup
from tomo.standard import *
from tomo.beamline.ID19.tools import *
from tomo.beamline.ID19.presets import OpiomPreset, DefaultChainOpiomPreset, PcoTomoOpiomPreset
from tomo.beamline.ID19.sequencepresets import FastShutterSequencePreset, MusstMaxAccuracySequencePreset

from fscan.fscantools import FScanMode
load_script("LATOMO.py")
load_script("MRTOMO.py")


def restore_default_chain(lima_detector):
    """Restore the default chain configuration"""
    if lima_detector is None:
        return
    det_mode = SimpleSetting(f'mode_{lima_detector.name}', default_value='SINGLE')
    det_trig = SimpleSetting(f'trig_{lima_detector.name}', default_value='INTERNAL_TRIGGER')
    if det_mode.get() == 'ACCUMULATION':
        lima_detector.acquisition.mode = 'ACCUMULATION'
        if det_trig.get() == 'EXTERNAL_TRIGGER_MULTI':
            DEFAULT_CHAIN.set_settings(MRTOMO_ext_chain_acc["chain_config"])
        if det_trig.get() == 'INTERNAL_TRIGGER':
            DEFAULT_CHAIN.set_settings(MRTOMO_int_chain_acc["chain_config"])
    else:
        lima_detector.acquisition.mode = 'SINGLE'
        if det_trig.get() == 'EXTERNAL_TRIGGER_MULTI':
            DEFAULT_CHAIN.set_settings(MRTOMO_ext_chain["chain_config"])
        if det_trig.get() == 'INTERNAL_TRIGGER':
            DEFAULT_CHAIN.set_settings(MRTOMO_int_chain["chain_config"])


tomo_config = latomo_config
tomo_config.set_active()

full_tomo = lafull_tomo

align_tomo = laalign_tomo

active_detector = full_tomo.get_controller_lima()

try:
    restore_default_chain(active_detector)
except:
    pass

fastshutter_preset = DefaultChainFastShutterPreset(exp_shutter)
DEFAULT_CHAIN.add_preset(fastshutter_preset)

#imagecorr_preset = DefaultChainImageCorrOnOffPreset(image_corr)
#DEFAULT_CHAIN.add_preset(imagecorr_preset)

try:
    SCAN_DISPLAY.auto=True
except NameError:
    # From Daiquiri Scan_DISPLAY is not defined, and we dont want Daiquiri to launch Flint
    pass


try:
    machinfo.initialize()
except:
    print("\nNo machine information available!\n")

referencemotorpreset = ReferenceMotorPreset(laref_displ)
fastshutterpreset = FastShutterPreset(exp_shutter)
darkshutterpreset = DarkShutterPreset(bsh2)

flat_scan=full_tomo.get_runner('flat')
flat_scan.add_scan_preset(referencemotorpreset,"motor_preset")

tomo_config.add_scan_preset(['flat', 'return_ref', 'CONTINUOUS', 'MULTITURNS'], fastshutterpreset)
tomo_config.add_scan_preset(['dark'], darkshutterpreset)

fscan = fscan_latomo.get_runner('fscan')
fscan.pars.motor = rl
fscan.pars.scan_mode = FScanMode.POSITION
fscan.pars.gate_mode = FScanMode.POSITION



print("")
print("Welcome to your new 'LATOMO' BLISS session !! ")
print("")
print("You can now customize your 'LATOMO' session by changing files:")
print("   * /LATOMO_setup.py ")
print("   * /LATOMO.yml ")
print("   * /scripts/LATOMO.py ")
print("")
