from bliss.setup_globals import *
from bliss.common import measurementgroup
from tomo.Presets import DefaultChainFastShutterPreset,DefaultChainImageCorrOnOffPreset

# set the default chain configuration
DEFAULT_CHAIN.set_settings(mhtomo.tomo_ccd.def_chain_single_mode['chain_config'])

# add shutter control to default scans
shutter_preset = DefaultChainFastShutterPreset(mhtomo.shutter)

DEFAULT_CHAIN.add_preset(shutter_preset)
imagecorr_preset = DefaultChainImageCorrOnOffPreset()
DEFAULT_CHAIN.add_preset(imagecorr_preset)

# Open Flint by default
SCAN_DISPLAY.auto=True

# initialise machine information
try:
    machinfo.initialize()
except:
    print("\nNo machine information available!\n")

# define alignment
from tomo.beamline.ID19.alignment import *

load_script("MHTOMO.py")

from id19.Tools import *
# define tomo object used for mvct, mvrct umvct, umvrct, cts functions    
init_tomo(mhtomo)

#apply the correct steps_per_unit to the current fucus motor
mhtomo.optic.focus_config_apply()

# define tomo object used for image correction object
image_corr.set_tomo(mhtomo)

print("")
print("Welcome to your new 'MHTOMO' BLISS session !! ")
print("")
print("You can now customize your 'MHTOMO' session by changing files:")
print("   * /MHTOMO_setup.py ")
print("   * /MHTOMO.yml ")
print("   * /scripts/MHTOMO.py ")
print("")

