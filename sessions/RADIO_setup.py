from bliss.setup_globals import *
from bliss.common import measurementgroup
from id19.presets.FastShutterChainPreset import FastShutterChainPreset
from id19.ImageCorrection import ImageCorrection
from id19.Tools import *

DEFAULT_CHAIN.set_settings(RADIO_ext_chain["chain_config"])

# add shutter control to default scans
shutter_preset = FastShutterChainPreset(exp_shutter)
DEFAULT_CHAIN.add_preset(shutter_preset)

# visual image correction
image_corr = ImageCorrection(pcolid19det1, exp_shutter, 0.1)

# Open Flint by default
SCAN_DISPLAY.auto=True

# initialise machine information
try:
    machinfo.initialize()
except:
    print("\nNo machine information available!\n")


load_script("RADIO.py")

print("")
print("Welcome to your new 'RADIO' BLISS session !! ")
print("")
print("You can now customize your 'RADIO' session by changing files:")
print("   * /RADIO_setup.py ")
print("   * /RADIO.yml ")
print("   * /scripts/RADIO.py ")
print("")
