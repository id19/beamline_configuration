from bliss.setup_globals import *
from bliss.common import measurementgroup
#from tomo.Presets import DefaultChainFastShutterPreset

# set the default chain configuration
#DEFAULT_CHAIN.set_settings(default_acq_chain['chain_config'])


# Open Flint by default
#SCAN_DISPLAY.auto=True
SCAN_DISPLAY.auto=False

# initialise machine information
try:
    machinfo.initialize()
except:
    print("\nNo machine information available!\n")

#
# XML server for external communication
#

#from bliss.setup_globals import *
#from bliss.common import standard

#from xmlrpc.server import SimpleXMLRPCServer
#import inspect
#import gevent

#xmlrpc_server = SimpleXMLRPCServer(("", 8000))
#xmlrpc_server.register_introspection_functions()

## register all standard functions to make them available via xml-rpc server
#for name, func in inspect.getmembers(standard, inspect.isfunction):
    #xmlrpc_server.register_function(func)

## start xml-rpc server in background
#gevent.spawn(xmlrpc_server.serve_forever)
#print ("XML RPC server started on port 8000")  
    
    

load_script("jens.py")

print("")
print("Welcome to your new 'jens' BLISS session !! ")
print("")
print("You can now customize your 'jens' session by changing files:")
print("   * /jens_setup.py ")
print("   * /jens.yml ")
print("   * /scripts/jens.py ")
print("")


