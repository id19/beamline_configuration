import fabio
import numpy
import gevent
import math
import time
from bliss import current_session
import os
from bliss.config.settings import SimpleSetting
from bliss.scanning.scan import ScanAbort
from bliss.scanning.group import ScanSequenceError
from bliss.scanning import scan_meta
from bliss.config import static
from bliss.shell.cli.user_dialog import UserChoice
from bliss.shell.cli.pt_widgets import BlissDialog
#from tomo.beamline.ID19.tools import umv
from bliss.controllers.lima.lima_base import Lima
from bliss.common.utils import BOLD
from tomo.beamline.ID19.presets import ReferenceMotorMoveInPreset, ReferenceMotorMoveOutPreset, WaitTriggerPreset

def SCAN_TIME():
    full_tomo.disable_sequence_presets()
    full_tomo.build_sequence()
    full_tomo.validate()
    full_tomo.enable_sequence_presets()
    runner = full_tomo.get_runner(full_tomo.pars.scan_type)
    if getattr(runner,'pars.display_flag',None) is not None:
        runner.pars.display_flag=False
    time_in_hh_mm_ss = str(
        datetime.timedelta(seconds=full_tomo._inpars.scan_time)
    ).split(":")
    hours = time_in_hh_mm_ss[0]
    minutes = time_in_hh_mm_ss[1]
    secondes = time_in_hh_mm_ss[2].split(".")[0]
    info_str = (
        "Estimated Scan Time: "
        + hours
        + " Hours "
        + minutes
        + " Minutes "
        + secondes
        + " Secondes"
    )
    print(info_str)     
    if getattr(runner,'pars.display_flag',None) is not None:
        runner.pars.display_flag=True

def ACC(acc_time=None, nb_subframes=None):   
    active_detector = full_tomo.get_controller_lima()
    if acc_time is None and nb_subframes is None:
        acc_time = active_detector.accumulation.max_expo_time
        nb_subframes = math.ceil(EXP_TIME()/acc_time)
        acc_time = EXP_TIME()/nb_subframes
    else:
        if nb_subframes == 1 or nb_subframes is None:
            if active_detector.acquisition.trigger_mode == 'INTERNAL_TRIGGER':
                set_int_trigger(active_detector)
            else:
                set_ext_trigger(active_detector)
            active_detector.acquisition.mode = 'SINGLE'
            nb_subframes = 1
        else:
            if active_detector.acquisition.trigger_mode == 'INTERNAL_TRIGGER':
                set_int_accumulation(active_detector)
            else:
                set_ext_accumulation(active_detector) 
            active_detector.acquisition.mode = 'ACCUMULATION'
        if acc_time is None:
            acc_time = active_detector.accumulation.max_expo_time
        if nb_subframes is None:
            nb_subframes = math.ceil(EXP_TIME()/acc_time)
            acc_time = EXP_TIME()/nb_subframes
        EXP_TIME(round(nb_subframes*acc_time,10))
        SUB_FRAME(acc_time)
    detectors_optic.get_tomo_detector(active_detector).acq_mode = active_detector.acquisition.mode
    print(f"number of subframes: {nb_subframes}")
    print(f"exposure time per subframe: {acc_time}")
    print(f'exposure time per frame: {full_tomo.config_pars.exposure_time}')

def fulltomo360(dataset_name=None,collection_name=None,start_pos=None,scan_info=None,run=True):
    full_tomo.full_turn_scan(dataset_name,collection_name,start_pos,scan_info,run)
    
def fulltomo180(dataset_name=None,collection_name=None,start_pos=None,scan_info=None,run=True):
    full_tomo.half_turn_scan(dataset_name,collection_name,start_pos,scan_info,run)

def fulltomo(start_pos=None,end_pos=None,tomo_n=None,expo_time=None,dataset_name=None,collection_name=None,scan_info=None,run=True):
    full_tomo.basic_scan(start_pos,end_pos,tomo_n,expo_time,dataset_name,collection_name,scan_info,run)

def activate_half_acqui():
    full_tomo.pars.half_acquisition = True
    
def deactivate_half_acqui():
    full_tomo.pars.half_acquisition = False
   
def activate_beam_check():
    full_tomo.pars.beam_check=True
    
def deactivate_beam_check():
    full_tomo.pars.beam_check=False
    
def activate_refill_check():
    full_tomo.pars.refill_check=True
    
def deactivate_refill_check():
    full_tomo.pars.refill_check=False
    
def activate_wait_for_beam():
    full_tomo.pars.wait_for_beam=True
    
def deactivate_wait_for_beam():
    full_tomo.pars.wait_for_beam=False



def zhelical(
    z_step,
    nb_turns,
    collection_name='',
    dataset_name="",
    tomo_n=None,
    z_start_pos=None,
    expo_time=None,
    sleep=0.0,
    cover=True,
):
    full_tomo.pars.start_pos = 0
    srot.sync_hard()
    nb_turns = nb_turns - 1
    z_helical.pars.dark_at_start=full_tomo.pars.dark_at_start
    z_helical.pars.dark_at_end=full_tomo.pars.dark_at_end
    z_helical.pars.dark_n=full_tomo.pars.dark_n
    z_helical.pars.flat_at_start=full_tomo.pars.flat_at_start
    z_helical.pars.flat_at_end=full_tomo.pars.flat_at_end
    z_helical.pars.flat_n=full_tomo.pars.flat_n
    z_helical.pars.half_acquisition=full_tomo.pars.half_acquisition
    z_helical.pars.images_on_return=full_tomo.pars.images_on_return
    z_helical.pars.return_to_start_pos=full_tomo.pars.return_to_start_pos
    z_helical.pars.latency_time=full_tomo.pars.latency_time
    z_helical.pars.scan_type=full_tomo.pars.scan_type
    z_helical.pars.comment=full_tomo.pars.comment
    z_helical.pars.wait_for_beam=full_tomo.pars.wait_for_beam
    z_helical.pars.refill_check=full_tomo.pars.refill_check
    z_helical.pars.beam_check=full_tomo.pars.beam_check
    z_helical.pars.nb_turns = nb_turns
    tomo_start_pos = full_tomo.pars.start_pos
    tomo_end_pos = tomo_start_pos + full_tomo.pars.range
    if z_start_pos is None:
        z_start_pos = z_helical._z_axis.position
    if tomo_n is None:
        tomo_n = full_tomo.pars.tomo_n
    if expo_time is None:
        expo_time = full_tomo.pars.exposure_time
    shopen()
    try:
        z_helical.basic_scan(z_step, nb_turns,  tomo_start_pos, tomo_n=tomo_n, expo_time=expo_time,collection_name=collection_name,dataset_name=dataset_name,z_start_pos=z_start_pos,cover=cover)
    finally:
        shclose()

# def zhelical(
    # z_step,
    # nb_turn,
    # collection_name='',
    # dataset_name="",
    # tomo_n=None,
    # z_start_pos=None,
    # expo_time=None,
    # sleep=0.0,
    # cover=True,
# ):
    # full_tomo.pars.start_pos = 0
    # if cover :
        # srot.sync_hard()
        # nb_turn = nb_turn - 1
        # z_helical.pars.dark_at_start=full_tomo.pars.dark_at_start
        # z_helical.pars.dark_at_end=full_tomo.pars.dark_at_end
        # z_helical.pars.dark_n=full_tomo.pars.dark_n
        # z_helical.pars.flat_at_start=full_tomo.pars.flat_at_start
        # z_helical.pars.flat_at_end=full_tomo.pars.flat_at_end
        # z_helical.pars.flat_n=full_tomo.pars.flat_n
        # z_helical.pars.half_acquisition=full_tomo.pars.half_acquisition
        # z_helical.pars.images_on_return=full_tomo.pars.images_on_return
        # z_helical.pars.return_to_start_pos=False
        # z_helical.pars.latency_time=full_tomo.pars.latency_time
        # z_helical.pars.scan_type=full_tomo.pars.scan_type
        # z_helical.pars.comment=full_tomo.pars.comment
        # z_helical.pars.wait_for_beam=full_tomo.pars.wait_for_beam
        # z_helical.pars.refill_check=full_tomo.pars.refill_check
        # z_helical.pars.beam_check=full_tomo.pars.beam_check
        # z_helical.pars.nturns = nb_turn
        # tomo_start_pos = full_tomo.pars.start_pos
        # tomo_end_pos = tomo_start_pos + full_tomo.pars.range
        # if z_start_pos is None:
            # z_start_pos = z_helical._z_axis.position
        # if tomo_n is None:
            # tomo_n = full_tomo.pars.tomo_n
        # if expo_time is None:
            # expo_time = full_tomo.pars.exposure_time
        # shopen()
        # try:
            # z_helical.basic_scan(tomo_start_pos, nb_turn, z_start_pos, z_step, tomo_n=tomo_n, expo_time=expo_time,collection_name=collection_name,dataset_name=dataset_name)
        # finally:
            # shclose()
    # else:
        # srot.sync_hard()
        # nb_turn = nb_turn - 1
        # helical.pars.dark_at_start=full_tomo.pars.dark_at_start
        # helical.pars.dark_at_end=full_tomo.pars.dark_at_end
        # helical.pars.dark_n=full_tomo.pars.dark_n
        # helical.pars.flat_at_start=full_tomo.pars.flat_at_start
        # helical.pars.flat_at_end=full_tomo.pars.flat_at_end
        # helical.pars.flat_n=full_tomo.pars.flat_n
        # helical.pars.half_acquisition=full_tomo.pars.half_acquisition
        # helical.pars.images_on_return=full_tomo.pars.images_on_return
        # helical.pars.return_to_start_pos=False
        # helical.pars.latency_time=full_tomo.pars.latency_time
        # helical.pars.scan_type=full_tomo.pars.scan_type
        # helical.pars.comment=full_tomo.pars.comment
        # helical.pars.wait_for_beam=full_tomo.pars.wait_for_beam
        # helical.pars.refill_check=full_tomo.pars.refill_check
        # helical.pars.beam_check=full_tomo.pars.beam_check
        # helical.pars.nturns = nb_turn
        # tomo_start_pos = full_tomo.pars.start_pos
        # tomo_end_pos = tomo_start_pos + full_tomo.pars.range
        # if z_start_pos is None:
            # z_start_pos = helical._z_axis.position
        # if tomo_n is None:
            # tomo_n = full_tomo.pars.tomo_n
        # if expo_time is None:
            # expo_time = full_tomo.pars.exposure_time
        # shopen()
        # try:
            # helical.basic_scan(tomo_start_pos, nb_turn, z_start_pos, z_step, tomo_n=tomo_n, expo_time=expo_time,collection_name=collection_name,dataset_name=dataset_name)
        # finally:
            # shclose()
    
def zseries(
    delta_pos,
    nb_scans,
    collection_name="",
    dataset_name="",
    sleep=0.0,
    start_pos=None,
    start_nb=1,
):
    tomo_start_pos = z_series.pars.start_pos
    tomo_end_pos = tomo_start_pos + z_series.pars.range
    tomo_n = z_series.pars.tomo_n
    expo_time = z_series.pars.exposure_time
    z_series.basic_scan(delta_pos,nb_scans,tomo_start_pos=tomo_start_pos,tomo_end_pos=tomo_end_pos,tomo_n=tomo_n,expo_time=expo_time,collection_name=collection_name,dataset_name=dataset_name,sleep=sleep,step_start_pos=start_pos,start_nb=start_nb)   

def one_collection_per_z():
    z_series.pars.one_collection_per_z = True
    z_series.pars.one_dataset_per_z = False
    
def one_dataset_per_z():
    z_series.pars.one_collection_per_z = False
    z_series.pars.one_dataset_per_z = True

def zseries_digits(value):
    z_series.pars.nb_digits = value

def alignment(save=False,plot=True,plot_images=True):
    full_tomo.init_sequence()
    if align_tomo.sequence._detector.acquisition.mode == 'ACCUMULATION':
        if align_tomo.sequence._detector.acquisition.trigger_mode == 'EXTERNAL_TRIGGER_MULTI':
            previous_chain_config = setup_globals.MRTOMO_ext_chain_acc["chain_config"]
        if align_tomo.sequence._detector.acquisition.trigger_mode == 'INTERNAL_TRIGGER':
            previous_chain_config = setup_globals.MRTOMO_int_chain_acc["chain_config"]
        set_int_accumulation(align_tomo.sequence._detector)
        align_tomo.sequence._detector.acquisition.trigger_mode = 'INTERNAL_TRIGGER'
    else:
        if align_tomo.sequence._detector.acquisition.trigger_mode == 'EXTERNAL_TRIGGER_MULTI':
            previous_chain_config = setup_globals.MRTOMO_ext_chain["chain_config"]
        if align_tomo.sequence._detector.acquisition.trigger_mode == 'INTERNAL_TRIGGER':
            previous_chain_config = setup_globals.MRTOMO_int_chain["chain_config"]
        set_int_trigger(align_tomo.sequence._detector)
        align_tomo.sequence._detector.acquisition.trigger_mode = 'INTERNAL_TRIGGER'
    if save:
        previous_collection = current_session.scan_saving.collection_name
        previous_dataset = current_session.scan_saving.dataset_name
        newcollection('align')
    _fastshutter_preset = DefaultChainFastShutterPreset(exp_shutter, close_shutter=False)
    DEFAULT_CHAIN.add_preset(_fastshutter_preset)
    _flat_runner = full_tomo._tomo_runners["flat"]
    _dark_runner = full_tomo._tomo_runners["dark"]
    _referencemotorpreset = _flat_runner._scan_presets["motor_preset"]
    _referencemotormovein = ReferenceMotorMoveInPreset(full_tomo._reference)
    _referencemotormoveout = ReferenceMotorMoveOutPreset(full_tomo._reference)
    _flat_runner.add_scan_preset(_referencemotormoveout,"referencemotormoveout")
    _dark_runner.add_scan_preset(_referencemotormovein, "referencemotormovein")
    _flat_runner.remove_scan_preset("motor_preset")
    full_tomo._reference.update_in_beam_position()
    try:
        print(BOLD(f"exposure time: {full_tomo.pars.exposure_time}"))
        align_tomo.align(save,plot,plot_images)
        full_tomo._reference.update_in_beam_position()
    finally:
        DEFAULT_CHAIN.set_settings(previous_chain_config)
        DEFAULT_CHAIN.remove_preset(_fastshutter_preset)
        if save:
            newcollection(previous_collection)
            newdataset(previous_dataset)
        _dark_runner.remove_scan_preset("referencemotormovein")
        _flat_runner.remove_scan_preset("referencemotormoveout")
        _flat_runner.add_scan_preset(_referencemotorpreset,"motor_preset")


def multiple_fulltomo(start_pos=0, tomo_range=360, tomo_n=None, expo_time=None, nb_tomo=1):

    if tomo_n is None:
        tomo_n = full_tomo.pars.tomo_n
    if expo_time is None:
        expo_time = full_tomo.pars.exposure_time
    prev_return_to_start_pos = full_tomo.pars.return_to_start_pos
    full_tomo.pars.return_to_start_pos = False
    end_pos = start_pos + nb_tomo*tomo_range
    prev_tomo_n = tomo_n
    tomo_n = nb_tomo*tomo_n
    try:    
        full_tomo.basic_scan(start_pos, end_pos, tomo_n, expo_time)
    finally:
        full_tomo.pars.tomo_n = prev_tomo_n
        full_tomo.pars.range = tomo_range
        pos = 360*round(srot.position/360)
        umv(srot,pos)
        srot.dial = 0
        srot.position = 0
        full_tomo.pars.return_to_start_pos = prev_return_to_start_pos



# def medtomo(start_pos=0, tomo_range=360, tomo_n=None, expo_time=None, nb_tomo=1):
    # if tomo_n is None:
        # tomo_n = multi_tomo.pars.tomo_n
    # if expo_time is None:
        # expo_time = multi_tomo.pars.exposure_time
    # prev_return_to_start_pos = multi_tomo.pars.return_to_start_pos
    # multi_tomo.pars.return_to_start_pos = False
    # end_pos = start_pos + tomo_range
    # try:
        # multi_tomo.pars.loop_mode = "EXTERNAL"    
        # multi_tomo.pars.sequence_trigger_mode = "EXTERNAL"    
        # multi_tomo.basic_scan(tomo_loop=nb_tomo, start_pos=start_pos,end_pos=end_pos,tomo_n=tomo_n,expo_time=expo_time)
    # finally:
        # pos = 360*round(medsrot.position/360)
        # umv(medsrot,pos)
        # medsrot.dial = 0
        # medsrot.position = 0
        # multi_tomo.pars.return_to_start_pos = prev_return_to_start_pos


def medtomoscan(start_pos, step_size, npoints, acq_time):
    _waittriggerpreset = WaitTriggerPreset(opiom2_tpg.opiom,2)
    medtomo = fscan_medtomo.get_runner('fscan')
    medtomo.pars.latency_time = 0
    medtomo.add_scan_preset(_waittriggerpreset,'_waittriggerpreset')
    medtomo(medsrot,start_pos,step_size,npoints,acq_time,0)
