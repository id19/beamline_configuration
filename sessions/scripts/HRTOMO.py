import os
import time
from bliss import current_session
import fabio
import numpy
import gevent
import math
from bliss.config.settings import SimpleSetting
from bliss.scanning.scan import ScanAbort
from bliss.controllers.lima.lima_base import Lima
from bliss.scanning.group import ScanSequenceError
from bliss.common.utils import BOLD



def set_ext_accumulation(detector):
    det_mode = SimpleSetting('mode_'+detector.name)
    det_trig = SimpleSetting('trig_'+detector.name)
    det_mode.set('ACCUMULATION')
    det_trig.set('EXTERNAL_TRIGGER_MULTI')
    setup_globals.DEFAULT_CHAIN.set_settings(
        setup_globals.HRTOMO_ext_chain_acc["chain_config"]
    )

    det_def_chain = setup_globals.DEFAULT_CHAIN._settings.get(detector)
    if det_def_chain is None:
        setup_globals.DEFAULT_CHAIN.set_settings(
            setup_globals.HRTOMO_ext_chain["chain_config"]
        )
        raise Exception(
            f"{detector.name} is not configured in default chain for accumulation"
        )


def set_ext_trigger(detector):
    det_mode = SimpleSetting('mode_'+detector.name)
    det_trig = SimpleSetting('trig_'+detector.name)
    det_mode.set('SINGLE')
    det_trig.set('EXTERNAL_TRIGGER_MULTI')
    setup_globals.DEFAULT_CHAIN.set_settings(
        setup_globals.HRTOMO_ext_chain["chain_config"]
    )

    det_def_chain = setup_globals.DEFAULT_CHAIN._settings.get(detector)
    if det_def_chain is None:
        raise Exception(f"{detector.name} is not configured in default chain")


def set_int_accumulation(detector):
    det_mode = SimpleSetting('mode_'+detector.name)
    det_trig = SimpleSetting('trig_'+detector.name)
    det_mode.set('ACCUMULATION')
    det_trig.set('INTERNAL_TRIGGER')
    setup_globals.DEFAULT_CHAIN.set_settings(
        setup_globals.HRTOMO_int_chain_acc["chain_config"]
    )

    det_def_chain = setup_globals.DEFAULT_CHAIN._settings.get(detector)
    if det_def_chain is None:
        setup_globals.DEFAULT_CHAIN.set_settings(
            setup_globals.HRTOMO_int_chain["chain_config"]
        )
        raise Exception(
            f"{detector.name} is not configured in default chain for accumulation"
        )


def set_int_trigger(detector):
    det_mode = SimpleSetting('mode_'+detector.name)
    det_trig = SimpleSetting('trig_'+detector.name)
    det_mode.set('SINGLE')
    det_trig.set('INTERNAL_TRIGGER')
    setup_globals.DEFAULT_CHAIN.set_settings(
        setup_globals.HRTOMO_int_chain["chain_config"]
    )

    det_def_chain = setup_globals.DEFAULT_CHAIN._settings.get(detector)
    if det_def_chain is None:
        raise Exception(f"{detector.name} is not configured in default chain")


def test_rotm():
    
    while True:
        try:
            tomopress.full_turn_scan()
            safe_move_rotm(180)
        except KeyboardInterrupt:
            break
        


def safe_move_rotm(rotm_pos):
    mv_rot = 1
    while mv_rot == 1:
        try:
            umv(rotm,rotm_pos)
            sleep(0.1)
            mv_rot = 0
        except:
            print("Trying again...%s"%time.ctime())


def test_dimax():
    while True:
        dimaxlinux.proxy.image_roi=[0,0,2016,2016]
        dimaxlinux.proxy.acq_nb_frames=6000
        dimaxlinux.proxy.acq_expo_time = 0.002
        dimaxlinux.proxy.prepareAcq()
        dimaxlinux.proxy.startAcq()
        while dimaxlinux.last.image_acquired != dimaxlinux.proxy.acq_nb_frames - 1:
            time.sleep(0.5)
            print('acquiring 6000')
        dimaxlinux.proxy.stopAcq()    
        dimaxlinux.proxy.image_roi=[504,504,1008,1008]
        dimaxlinux.proxy.acq_nb_frames=24000
        dimaxlinux.proxy.acq_expo_time = 0.002
        dimaxlinux.proxy.prepareAcq()
        dimaxlinux.proxy.startAcq()
        while dimaxlinux.last.image_acquired != dimaxlinux.proxy.acq_nb_frames - 1:
            time.sleep(0.5)
            print('acquiring 24000')
        dimaxlinux.proxy.stopAcq() 
        dimaxlinux.proxy.image_roi=[0,0,2016,2016]
        dimaxlinux.proxy.acq_nb_frames=6000
        dimaxlinux.proxy.acq_expo_time = 0.002
        dimaxlinux.proxy.prepareAcq()
        dimaxlinux.proxy.startAcq()
        while dimaxlinux.last.image_acquired != dimaxlinux.proxy.acq_nb_frames - 1:
            time.sleep(0.5)
            print('acquiring 6000')
        dimaxlinux.proxy.stopAcq()    
        dimaxlinux.proxy.image_roi=[756,756,504,504]
        dimaxlinux.proxy.acq_nb_frames=48000
        dimaxlinux.proxy.acq_expo_time = 0.002
        dimaxlinux.proxy.prepareAcq()
        dimaxlinux.proxy.startAcq()
        while dimaxlinux.last.image_acquired != dimaxlinux.proxy.acq_nb_frames - 1:
            time.sleep(0.5)
            print('acquiring 48000')
        dimaxlinux.proxy.stopAcq() 
            


