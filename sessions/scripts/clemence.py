from bliss import current_session, global_map
from bliss.scanning.scan import ScanState
from bliss.scanning.scan import ScanAbort
import logging
import numpy

_logger = logging.getLogger(__name__)

# def test(
    # nb_scans
# ):
    # config.get('mrxc')
    # user_aborted = False
    # try:   
        # for i in range(nb_scans):
            # try:
                # ascan(mrxc,0,10,10,1,sim_counter)
            # except (KeyboardInterrupt, ScanAbort):
                # user_aborted = True
                # raise
            # except Exception:
                # _logger.error("error acquired during first try",exc_info=True)
                # try:
                    # print( "SCAN CRASHED WE REDO IT")
                    # ascan(mrxc,0,10,10,1,sim_counter)
                # except Exception:
                    # _logger.error("error acquired during second try",exc_info=True)
                    # raise
            # finally:
                # try:
                    # if not user_aborted:
                        # print("BEAM LOST WE REDO THE SCAN")
                        # ascan(mrxc,0,10,10,1,sim_counter)
                # except:
                    # _logger.error("error acquired during beam lost retry",exc_info=True)
                    # raise
    # finally:
        # print("end")

def zseries(
    delta_pos,
    nb_scans,
    collection_name="",
    dataset_name="",
    sleep=0.0,
    start_pos=None,
    start_nb=1,
):
    srot.sync_hard()
    tomo_start_pos = z_series.pars.start_pos
    tomo_end_pos = tomo_start_pos + z_series.pars.range
    tomo_n = z_series.pars.tomo_n
    expo_time = z_series.pars.exposure_time
    z_series.basic_scan(delta_pos,nb_scans,tomo_start_pos=tomo_start_pos,tomo_end_pos=tomo_end_pos,tomo_n=tomo_n,expo_time=expo_time,collection_name=collection_name,dataset_name=dataset_name,sleep=sleep,step_start_pos=start_pos,start_nb=start_nb)


def shclose():
    print("close shutter")
    
def shopen():
    print("open shutter")
    
def feclose():
    print("close frontend")
    
def feopen():
    print("open frontend")

def set_ext_accumulation(detector):
    DEFAULT_CHAIN.set_settings(
        clemence_chain_acc["chain_config"]
    )


def set_ext_trigger(detector):
    DEFAULT_CHAIN.set_settings(
        []
    )


def set_int_accumulation(detector):
    DEFAULT_CHAIN.set_settings(
        clemence_chain_acc["chain_config"]
    )

    det_def_chain = DEFAULT_CHAIN._settings.get(detector)
    if det_def_chain is None:
        DEFAULT_CHAIN.set_settings(
            []
        )
        raise Exception(
            f"{detector.name} is not configured in default chain for accumulation"
        )


def set_int_trigger(detector):
    DEFAULT_CHAIN.set_settings(
        []
    )
 
class FakeMachInfo:

    def __init__(self):
        self.check = False


import h5py
import pint 

def _is_tomo_sequence(h5entry):
    if h5entry.get('technique').get('subscans') is not None:
        return True

def load_from_h5(path):
    with h5py.File(path,'r') as myfile:
        tomosequences = []
        for scan in myfile.keys():
            if _is_tomo_sequence(myfile[scan]):
                tomosequences.append(myfile[scan])
        if len(tomosequences) > 1:
            #has to be handle
            pass
        else:
            tomosequence = tomosequences[0]
            tomopars = dict()
            for key in tomosequence['technique']['scan'].keys():
                if isinstance(tomosequence['technique']['scan'][key][()],bytes):
                    tomopars[key] = tomosequence['technique']['scan'][key][()].decode('utf-8')
                elif isinstance(tomosequence['technique']['scan'][key][()], numpy.int64):
                    tomopars[key] = int(tomosequence['technique']['scan'][key][()])
                elif isinstance(tomosequence['technique']['scan'][key][()], numpy.float64):
                    tomopars[key] = float(tomosequence['technique']['scan'][key][()])
                    if 'time' in key and tomosequence['technique']['scan'][key].attrs.get('units') is not None:
                        units = tomosequence['technique']['scan'][key].attrs['units']
                        tomopars[key] = pint.Quantity(tomopars[key],units).to('s').magnitude
                else:
                    tomopars[key] = tomosequence['technique']['scan'][key][()]
                if key in full_tomo.pars.to_dict().keys():
                    copy_pars = full_tomo.pars.to_dict()
                    mydict = {key: tomopars[key]}
                    copy_pars.update(mydict)
                    full_tomo.pars.from_dict(copy_pars)
            for key in tomosequence['technique']['scan_flags'].keys():
                if 'ref' or 'dark' in key:
                    mydict = {key: tomopars[key]}
                
                    
                
    return tomopars
