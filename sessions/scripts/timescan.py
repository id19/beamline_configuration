import gevent

global timescan_task
def start_timescan(count_time):
    global timescan_task
    ts = timescan(count_time,run=False)
    timescan_task = gevent.spawn(ts.run)

def stop_timescan():
    global timescan_task
    pcolinux.proxy.abortAcq()
    gevent.killall([timescan_task])
