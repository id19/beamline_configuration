import os
import gevent
import time
global task
import fabio
import numpy
import math
from bliss import current_session
from bliss.config.settings import SimpleSetting
from bliss.scanning.scan import ScanAbort
from bliss.scanning.group import ScanSequenceError
from bliss.scanning import scan_meta
from bliss.config import static
from bliss.shell.cli.user_dialog import UserChoice
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.controllers.lima.lima_base import Lima
from bliss.common.utils import BOLD
from bliss.scanning.toolbox import ChainBuilder

    
def test_bnc(detector):
    set_ext_trigger(detector)
    ct(0.1,detector)


def deactivate_shutter_close():
    global fastshutter_preset
    DEFAULT_CHAIN.remove_preset(fastshutter_preset)
    fastshutter_preset = DefaultChainFastShutterPreset(beam_shutter, close_shutter=False)
    DEFAULT_CHAIN.add_preset(fastshutter_preset)

def activate_shutter_close():
    global fastshutter_preset
    DEFAULT_CHAIN.remove_preset(fastshutter_preset)
    fastshutter_preset = DefaultChainFastShutterPreset(beam_shutter, close_shutter=True)
    DEFAULT_CHAIN.add_preset(fastshutter_preset)

def autoflat():
    image_corr.take_flat()
    image_corr.take_dark()
    image_corr.dark_on()
    image_corr.flat_on()

def noflat():
    image_corr.flat_off()
    image_corr.dark_off()
    
def flatct(exposure_time):
    noflat()
    active_detector = get_controller_lima()
    proj_scan = sct(exposure_time,save=False)
    proj_current = proj_scan.get_data()['machinfo:current'][-1]
    proj_image = proj_scan.get_data()[f'{active_detector.name}:image']
    with fabio.open(image_corr.dark_image) as i:
            dark_image = i.data
    with fabio.open(image_corr.flat_image) as i:
            flat_image = i.data
    flat_current = image_corr.flat_current
    
    image = (((proj_image - dark_image)/proj_current)*flat_current)/(flat_image-dark_image)
    image= numpy.where(numpy.isnan(image),0,image)
    image = numpy.where(numpy.isinf(image),0,image)[0]
    
    f = flint()
    p = f.get_plot("image", name="image_autoflat",  unique_name="image_autoflat", selected=True, closeable=True)

    p.set_data(image.reshape(image_corr.detector.image.height,image_corr.detector.image.width))
    p.yaxis_direction = "down"
    p.side_histogram_displayed = False
    
    image_corr.dark_on()
    image_corr.flat_on()
    ct(exposure_time)

def shopen():
    bsh2.open()


def shclose():
    bsh2.close()


def feopen():
    frontend.open()


def feclose():
    frontend.close()


def psp():

    print(f'umv(sx, {sx.position} ,sy, {sy.position}, sz, {sz.position}, yrot, {yrot.position})') 

def ps():
    wm(psvo,psvg,psho,pshg)

def ss():
    wm(ssvo,ssvg,ssho,sshg)

def lsdef(name=None):
    if name is None:
        return [x.__name__ for x in globals().values() if inspect.isfunction(x)]
    return [
        x.__name__
        for x in globals().values()
        if inspect.isfunction(x) and name in x.__name__.lower()
    ]


def read_opiom2_input(i):
    opiom2_tpg = get_config().get("opiom2_tpg")
    all_i = opiom2_tpg.opiom.comm("?I")
    i = int(all_i,16) >> (i-1) & 0x1
    return i 


def send_trigger_opiom2_output(o):
    width = 100000 #100ms
    delay = 100000
    opiom2_tpg = get_config().get("opiom2_tpg")
    opiom2_tpg.opiom.comm_ack(f"CNT {o} RESET")
    cmd = f"CNT {o} CLK8 RISE D32 PULSE "+str(width)+" "+str(delay)+" "+str(1)
    opiom2_tpg.opiom.comm_ack(cmd)
    opiom2_tpg.opiom.comm_ack(f"CNT {o} START")
