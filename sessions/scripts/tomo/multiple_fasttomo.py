from bliss.setup_globals import *
from bliss import setup_globals, current_session


def multiple_fasttomo(total_time, start_pos=0, tomo_range=360, tomo_n=None, expo_time=None, dataset_name=None, run=True):
    tomo = getattr(setup_globals,[obj for obj in current_session.object_names if 'tomo' in obj and 'pco' not in obj and 'multiplexer' not in obj][0])
    
    end_pos = start_pos + tomo_range
    if tomo_n is None:
        tomo_n = tomo.parameters.tomo_n
    if expo_time is None:
        expo_time = tomo.parameters.exposure_time
    tomo.fasttomo.prepare()    
    one_time_scan = tomo.tomo_tools.continuous_scan_time(tomo.rotation_axis,start_pos,end_pos)
    nb_scans = int(total_time*60/one_time_scan)+1
        
    step_size = tomo_range / tomo_n
    prev_end = end_pos
    end_pos = start_pos + nb_scans*tomo_range
    prev_tomo_n = tomo_n
    tomo_n = nb_scans*tomo_n
    try:    
        tomo.basic_scan(start_pos, end_pos, tomo_n, expo_time, dataset_name=dataset_name, run=run)
    finally:
        tomo.parameters.tomo_n = prev_tomo_n
        tomo.parameters.end_pos = prev_end
        srot.dial = srot.dial%360
        srot.position = srot.position%360
    
