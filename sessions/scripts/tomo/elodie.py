from bliss.setup_globals import *

def init_scan():
   # images of one scan 
   hrpcotomo.parameters.tomo_n = 1000
   # number of scans
   hrpcotomo.parameters.ntomo = 5
   # time of one scan
   hrpcotomo.parameters.time = 1 
   # waiting turns
   hrpcotomo.parameters.nwait = 0 
   hrpcotomo.parameters.start_pos = 5
   
   set_centered_roi(1008,1008)

def set_centered_roi(width,height):
    
    #remove roi to get image full size
    hrpcotomo.tomo_ccd.detector.proxy.image_roi = [0,0,0,0]
    full_width = hrpcotomo.tomo_ccd.detector.proxy.image_roi[2]
    full_height = hrpcotomo.tomo_ccd.detector.proxy.image_roi[3]
    roix = int((full_width - width)/2)
    roiy = int((full_height - height)/2)
    hrpcotomo.tomo_ccd.detector.image.roi = [roix,roiy,width,height]
    

def test_consecutive():
    
    init_scan()
    
    hrpcotomo.setup_info('consecutive')
    
    hrpcotomo.consecutive_tomo(hrpcotomo.parameters.start_pos,
                               hrpcotomo.parameters.tomo_n,
                               hrpcotomo.parameters.time,
                               hrpcotomo.parameters.ntomo)

def test_multiple():
    
    init_scan()
    
    hrpcotomo.setup_info('multiple')
    
    hrpcotomo.multiple_tomo(hrpcotomo.parameters.start_pos,
                               hrpcotomo.parameters.tomo_n,
                               hrpcotomo.parameters.time,
                               hrpcotomo.parameters.nloop,
                               hrpcotomo.parameters.nwait)

def test_endless():
    
    init_scan()
    
    hrpcotomo.setup_info('endless')
    
    hrpcotomo.multiple_tomo(hrpcotomo.parameters.start_pos,
                               hrpcotomo.parameters.tomo_n,
                               hrpcotomo.parameters.time,
                               hrpcotomo.parameters.nloop,
                               hrpcotomo.parameters.nwait)
