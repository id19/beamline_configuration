from bliss.setup_globals import *
import math
import time
import os
import numpy as np
from bliss.scanning.scan import ScanState

from PyTango import DeviceProxy

# =============================================================================
rootdir = '/data/id19/inhouse/tmp/HADES_Comfile'
NAfileVal= rootdir+'/TXTFiles/Val.txt'
NAfilePt = rootdir+'/TXTFiles/PtScan.txt'
NAxlHADES= rootdir+'/Excelfiles/Com.xlsx'
NAfileIni = rootdir+'/TXTFiles/Init_par.txt'
tango_server = DeviceProxy('id19/hades-press/1')
# =============================================================================

def test_com_hades(tomo):
    #check align is yrot 0
    
    tomo_range = 180
    
    with open(NAfileIni) as file:
        all_data = file.readlines()
        numscan =  int(all_data[0].split('\t')[-1])
        nlscan  =  float(all_data[1].split('\t')[-1])
        MaxP    =  float(all_data[2].split('\t')[-1])
        IniP    =  float(all_data[3].split('\t')[-1])
        OPMODEPa =  int(all_data[4].split('\t')[-1])
        threshold =  float(all_data[5].split('\t')[-1])
        dP    =  float(all_data[6].split('\t')[-1])
        doubleP    =  float(all_data[7].split('\t')[-1])
        doubleP_value    =  float(all_data[8].split('\t')[-1])
    
    
    #if OPMODEPa == 1:
        ## DEFINE RAMPING LAW
        #lpressure = sorted(MaxP-(MaxP-IniP)*np.power((np.linspace(0,1,numscan)),nlscan))
    
    #if OPMODEPa == 2:
        #dPlist = [5,2,1,0.5,0.1]
        ## Pathre = [0.5,0.7,0.8,0.9,1.5]
        #Pathre = [0.7,0.85,0.9,0.95,1.5]
        #Palist = np.arange(IniP, Pathre[0]*MaxP, dPlist[0])
        #for ii in range(np.size(dPlist)):
            #tmplist = np.arange(Pathre[ii-1]*MaxP, Pathre[ii]*MaxP, dPlist[ii])
            #Palist = [*Palist, *tmplist]

    #for pressure in lpressure:
    i = 0
    while True:
        try:
            scan_lock = 1
            while scan_lock == 1:
                print('HADES is running')
                scan_lock = tango_server.ScanLock
                time.sleep(5)

            lvdt = tango_server.LVDT
            if lvdt > threshold:
                print('Sample catastrophic failure. Abort scans')
                tango_server.ScanLock = 1
                raise
                break
                
            if not machinfo.check:        
                #activate check beam 
                machinfo.check=True
            
            
            exception = False
            
            ###temporary workaround to avoid redis get data gevent timeout
            nb_keys = os.popen('ssh -X blissadm@0 "conda activate bliss_dev; redis-cli -p 25002 -n 1 DBSIZE"').read()
            if int(nb_keys) >= 5000:
                while not 'ON' in str(SCAN_SAVING.writer_object.proxy.state()):
                    time.sleep(0.5)
                os.system('ssh -X blissadm@0 "conda activate bliss_dev; redis-cli -p 25002 -n 1 FLUSHDB"')
                bsh2.close()
                ct(0.05)
            ###
            
            #if (i%50 == 0 and not os.path.exists('/'.join(SCAN_SAVING.data_fullpath.split('/')[:-2])+'/'+SCAN_SAVING.collection_name+'_ref_')) or (i == 0 and SCAN_SAVING.dataset.name == 'ref_failed'):
            if (i%50 == 0 or (i == 0 and SCAN_SAVING.dataset.name == 'ref_failed')):
                #reference scan
                umv(sy,5)
                
                SCAN_SAVING.newdataset('ref')
                try:
                    bsh2.open()
                    if tomo_range == 180:
                        tomo.half_turn_scan()
                    if tomo_range == 360:
                        tomo.full_turn_scan()
                    bsh2.close()
                except:
                    exception = True
                    SCAN_SAVING.newdataset('ref_failed_')
                    raise
                    break
            
            if sy.position != 0:
                umv(sy,0)
            
            pressure = tango_server.Pressure
            
            already_double = False
            for directory in os.listdir('/'.join(SCAN_SAVING.data_fullpath.split('/')[:-2])):
                if 'DVC' in directory:
                    already_double = True
            if doubleP and pressure > doubleP_value and not already_double:
                nb_scans = 2
            else:
                nb_scans = 1
            
            SCAN_SAVING.newdataset('P_'+str(pressure).replace('.','_')+'_D_'+str(round(lvdt)))
            
            for nb in range(nb_scans):
                
                
                if nb == 1:
                    SCAN_SAVING.newdataset('P_'+str(pressure).replace('.','_')+'_D_'+str(round(lvdt))+'_'+'DVC')
                try:
                    bsh2.open()
                    if tomo_range == 180:
                        tomo.half_turn_scan()
                    if tomo_range == 360:
                        tomo.full_turn_scan()
                    bsh2.close()
                    
                    scan_failure = 0    
                except Exception as exc:
                    
                    if exc == KeyboardInterrupt:
                        exception = True
                        raise
                        break
                                
                    scan_failure = 1     
                    
                    while scan_failure < 10 and SCANS[-1].state != ScanState.DONE:
                        try:
                            print('try again')
                            time.sleep(5)
                            bsh2.open()
                            if tomo_range == 180:
                                tomo.half_turn_scan()
                            if tomo_range == 360:
                                tomo.full_turn_scan()
                            bsh2.close()
                        except Exception as exc:
                            if exc == KeyboardInterrupt:
                                exception = True
                                raise
                                break
                            else:
                                scan_failure += 1
                        
                finally:
                    bsh2.close()
                    
                    if scan_failure == 10:
                        msg = 'scan failed after 10 trials'
                        subj = 'hades'
                        os.system(f"echo {msg} | mail -s {subj} cordonnier.benoit@gmail.com")
                        exception = True
                        raise
                        break
            if not exception:           
                tango_server.ScanLock = 1
        
            i += 1
        except:
            raise
            break
    #deactivate check beam 
    machinfo.check=False
    
