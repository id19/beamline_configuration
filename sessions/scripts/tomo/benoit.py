from bliss.setup_globals import *
import math
import time
import os
import numpy as np
from bliss.scanning.scan import ScanState

from PyTango import DeviceProxy

# =============================================================================
rootdir = '/data/id19/inhouse/tmp/HADES_Comfile'
NAfileVal= rootdir+'/TXTFiles/Val.txt'
NAfilePt = rootdir+'/TXTFiles/PtScan.txt'
NAxlHADES= rootdir+'/Excelfiles/Com.xlsx'
NAfileIni = rootdir+'/TXTFiles/Init_par.txt'
tango_server = DeviceProxy('id19/hades-press/1')
# =============================================================================


def test_mapping():
    
    start_posZ = 10
    end_posZ = 26
    start_posY = 0.79
    end_posY = -1.71

    pixsize = hrtomo.tomo_ccd.calculate_image_pixel_size(hrtomo.optic.magnification)*1E-3
    imdim = hrtomo.tomo_ccd.detector.image.max_dim 
    nb_step = math.ceil(max(abs((end_posZ - start_posZ)/pixsize/(imdim[0]*.875)),abs((end_posY - start_posY)/pixsize/(imdim[1]*.875))))

    
    

    tomo_range = 360
    breakpoint()
    positionsZ = [p for p in range(start_posZ,end_posZ,int((end_posZ-start_posZ)/nb_step))]
    positionsY = [p for p in range(start_posY,end_posY,int((end_posY-start_posY)/nb_step))]
    
    for step in range(0,nb_step):
        
        umv(sy,positionsY[step],sz,positionsZ[step])
    

#        if tomo_range == 180:
#            hrtomo.half_turn_scan()
#        if tomo_range == 360:
#            hrtomo.full_turn_scan()


def test_com_hades(tomo):
    #check align is yrot 0
    
    tomo_range = 180
    
    with open(NAfileIni) as file:
        all_data = file.readlines()
        numscan =  int(all_data[0].split('\t')[-1])
        nlscan  =  float(all_data[1].split('\t')[-1])
        MaxP    =  float(all_data[2].split('\t')[-1])
        IniP    =  float(all_data[3].split('\t')[-1])
        OPMODEPa =  int(all_data[4].split('\t')[-1])
        threshold =  float(all_data[5].split('\t')[-1])
        dP    =  float(all_data[6].split('\t')[-1])
        #doubleP    =  float(all_data[7].split('\t')[-1])
        #doubleP_value    =  float(all_data[8].split('\t')[-1])
    
    
    #if OPMODEPa == 1:
        ## DEFINE RAMPING LAW
        #lpressure = sorted(MaxP-(MaxP-IniP)*np.power((np.linspace(0,1,numscan)),nlscan))
    
    #if OPMODEPa == 2:
        #dPlist = [5,2,1,0.5,0.1]
        ## Pathre = [0.5,0.7,0.8,0.9,1.5]
        #Pathre = [0.7,0.85,0.9,0.95,1.5]
        #Palist = np.arange(IniP, Pathre[0]*MaxP, dPlist[0])
        #for ii in range(np.size(dPlist)):
            #tmplist = np.arange(Pathre[ii-1]*MaxP, Pathre[ii]*MaxP, dPlist[ii])
            #Palist = [*Palist, *tmplist]

    #for pressure in lpressure:
    i = 0
    while True:
        try:
            scan_lock = 1
            while scan_lock == 1:
                print('HADES is running')
                scan_lock = tango_server.ScanLock
                time.sleep(5)

            lvdt = tango_server.LVDT
            if lvdt > threshold:
                print('Sample catastrophic failure. Abort scans')
                raise
                break
            
            if not machinfo.check:        
                #activate check beam 
                machinfo.check=True
            
            #if i == 0 and not os.path.exists('/'.join(SCAN_SAVING.data_fullpath.split('/')[:-2])+'/'+SCAN_SAVING.sample+'_ref_'):
            if i == 0:
                #reference scan
                umv(sy,5)
                
                SCAN_SAVING.newdataset('ref_')
                try:
                    bsh2.open()
                    if tomo_range == 180:
                        tomo.half_turn_scan()
                    if tomo_range == 360:
                        tomo.full_turn_scan()
                    bsh2.close()
                except:
                    raise
                    break
            
            if sy.position != 0:
                umv(sy,0)
            
            pressure = tango_server.Pressure
            
            #already_double = False
            #for directory in os.listdir('/'.join(SCAN_SAVING.data_fullpath.split('/')[:-2])):
            #    if 'DVC' in directory:
            #        already_double = True
            #if doubleP and pressure > doubleP_value and not already_double:
                #nb_scans = 2
            #else:
                #nb_scans = 1
            
            #SCAN_SAVING.newdataset('P_'+str(pressure).replace('.','_')+'_D_'+str(round(lvdt)))
            SCAN_SAVING.newdataset('P_'+str(pressure).replace('.','_'))
            
            #for nb in range(nb_scans):
                
                #if nb == 1:
                    #SCAN_SAVING.newdataset('P_'+str(pressure).replace('.','_')+'_D_'+str(round(lvdt))+'_'+'DVC')
            try:
                bsh2.open()
                if tomo_range == 180:
                    tomo.half_turn_scan()
                if tomo_range == 360:
                    tomo.full_turn_scan()
                bsh2.close()
                
                scan_failure = 0    
            except:
                
                if SCANS[-1].state == ScanState.USER_ABORTED:
                    raise
                    break
                    
                scan_failure = 1     
                
                while scan_failure < 10 and tomo.tomo_ccd.detector.last.image_acquired + 1 != tomo.tomo_ccd.detector.acquisition.nb_frames:
                    try:
                        msg = f'scan failed SCAN[-1].state:{SCAN[-1].state}'
                        subj = 'hades'
                        os.system(f"echo {msg} | mail -s {subj} clemence.muzelle@gmail.com")
                        bsh2.open()
                        if tomo_range == 180:
                            tomo.half_turn_scan()
                        if tomo_range == 360:
                            tomo.full_turn_scan()
                        bsh2.close()
                            
                    except:
                        if SCANS[-1].state == ScanState.USER_ABORTED:
                            raise
                            break
                        else:
                            scan_failure += 1
                    
            finally:
                
                if SCANS[-1].state == ScanState.USER_ABORTED:
                    raise
                    break
            
                if scan_failure == 10:
                    msg = 'scan failed after 10 trials'
                    subj = 'hades'
                    os.system(f"echo {msg} | mail -s {subj} cordonnier.benoit@gmail.com")
                    raise
                    break
                        
            tango_server.ScanLock = 1
        
            i += 1
        except:
            break
    #deactivate check beam 
    machinfo.check=False
