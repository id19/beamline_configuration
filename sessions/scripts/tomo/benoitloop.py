import time
from bliss.setup_globals import *
import os

def benfruitloop(tomo):
    #check align is yrot 0
    while True:
        try:
            ###temporary workaround to avoid redis get data gevent timeout
            nb_keys = os.popen('ssh -X blissadm@0 "conda activate bliss_dev; redis-cli -p 25002 -n 1 DBSIZE"').read()
            if int(nb_keys) >= 5000:
                while not 'ON' in str(SCAN_SAVING.writer_object.proxy.state()):
                    time.sleep(0.5)
                os.system('ssh -X blissadm@0 "conda activate bliss_dev; redis-cli -p 25002 -n 1 FLUSHDB"')
                bsh2.close()
                ct(0.05)
            ###
            
            umv(sy,0)
            print('launching scan')
            bsh2.open()
            tomo.half_turn_scan()
            bsh2.close()
            time.sleep(120)
        except Exception as exc:
            if exc == KeyboardInterrupt:
                exception = True
                raise
                break
            else:
                bsh2.close()
                srot.on()
                srot.sync_hard() 
                pass
