from bliss.common import session
from bliss import setup_globals


def set_ext_accumulation(detector, acc_max_expo_time=None):
    """Set default chain for external trigger and ACCUMULATION mode
    """
    
    # Create the names of SINGLE and ACCUMULATION chaines
    def_chain_name = ("setup_globals.%s_ext_chain" % session.CURRENT_SESSION.name)
    def_chain = eval(def_chain_name)
    
    acc_chain_name = ("%s_acc" % def_chain_name)
    acc_chain = eval(acc_chain_name)
    
    # set the ACCUMULATION chain
    setup_globals.DEFAULT_CHAIN.set_settings(acc_chain['chain_config'])
    
    # Is the detector handled?
    det_def_chain = setup_globals.DEFAULT_CHAIN._settings.get(detector)
    if det_def_chain is None:
        setup_globals.DEFAULT_CHAIN.set_settings(def_chain['chain_config'])
        raise Exception(f'{detector.name} is not configured in external trigger chain for accumulation')
    
    # set the accumulation max. eposure time on the detector
    if acc_max_expo_time != None:
        detector.accumulation.max_expo_time = acc_max_expo_time
    
    print ("External Trigger + Accumulation:")
    print ("Detector:      %s" % detector.name)
    print ("Max expo time: %f\n" % detector.accumulation.max_expo_time)



def set_ext_single(detector):
    """Set default chain for external trigger and SINGLE mode
    """
    
    # Create the name of the SINGLE chain
    def_chain_name = ("setup_globals.%s_ext_chain" % session.CURRENT_SESSION.name)
    def_chain = eval(def_chain_name)
    
    # set the SINGLE chain
    setup_globals.DEFAULT_CHAIN.set_settings(def_chain['chain_config'])
    
    # Is the detector handled?
    det_def_chain = setup_globals.DEFAULT_CHAIN._settings.get(detector)
    if det_def_chain is None:
        raise Exception(f'{detector.name} is not configured in external trigger chain') 

    print ("External Trigger:")
    print ("Detector: %s\n" % detector.name)
    
    
def set_int_single(detector):
    """Set default chain for internal (software) trigger and SINGLE mode
    """
    
    # Create the name of the SINGLE chain
    def_chain_name = ("setup_globals.%s_int_chain" % session.CURRENT_SESSION.name)
    def_chain = eval(def_chain_name)
    
    # set the SINGLE chain
    setup_globals.DEFAULT_CHAIN.set_settings(def_chain['chain_config'])
    
    # Is the detector handled?
    det_def_chain = setup_globals.DEFAULT_CHAIN._settings.get(detector)
    if det_def_chain is None:
        raise Exception(f'{detector.name} is not configured in internal trigger chain') 

    print ("Internal Trigger:")
    print ("Detector: %s\n" % detector.name)
    
    
def set_int_accumulation(detector, acc_max_expo_time=None):
    """Set default chain for internal (software) trigger and ACCUMULATION mode
    """
    
    # Create the names of SINGLE and ACCUMULATION chaines
    def_chain_name = ("setup_globals.%s_int_chain" % session.CURRENT_SESSION.name)
    def_chain = eval(def_chain_name)
    
    acc_chain_name = ("%s_acc" % def_chain_name)
    acc_chain = eval(acc_chain_name)
    
    # set the ACCUMULATION chain
    setup_globals.DEFAULT_CHAIN.set_settings(acc_chain['chain_config'])
    
    # Is the detector handled?
    det_def_chain = setup_globals.DEFAULT_CHAIN._settings.get(detector)
    if det_def_chain is None:
        setup_globals.DEFAULT_CHAIN.set_settings(def_chain['chain_config'])
        raise Exception(f'{detector.name} is not configured in internal trigger chain for accumulation')
    
    # set the accumulation max. eposure time on the detector
    if acc_max_expo_time != None:
        detector.accumulation.max_expo_time = acc_max_expo_time
    
    print ("Internal Trigger + Accumulation:")
    print ("Detector:      %s" % detector.name)
    print ("Max expo time: %f\n" % detector.accumulation.max_expo_time)
    
    
def get_trigger_status(detector):
    """ Print the current trigger status and acquisition mode
    """
    
    det_def_chain = setup_globals.DEFAULT_CHAIN._settings.get(detector)
    if det_def_chain is None:
        raise Exception(f'{detector.name} is not configured in the default chain')
        
    try:
        acq_trigger_mode = det_def_chain['acquisition_settings']['acq_trigger_mode']
    except:
        acq_trigger_mode = 'INTERNAL_TRIGGER_MULTI'
    try:
        acq_mode = det_def_chain['acquisition_settings']['acq_mode']
    except:
        acq_mode = 'SINGLE'
    try:
        master           = det_def_chain['master'].name
    except:
        master = 'None'
    
    print ("Default Chain Configuration:")
    print ("Detector:         %s" % detector.name)
    print ("acq_trigger_mode: %s" % acq_trigger_mode)
    print ("acq_mode:         %s" % acq_mode)
    print ("master:           %s" % master)
