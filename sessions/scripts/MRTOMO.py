#from bliss.setup_globals import *
import os
import time
from bliss import current_session
import fabio
import numpy
import gevent
import math
from bliss.config.settings import SimpleSetting
from bliss.scanning.scan import ScanAbort
from bliss.controllers.lima.lima_base import Lima
from bliss.scanning.group import ScanSequenceError
from bliss.common.utils import BOLD


def set_ext_accumulation(detector):
    det_mode = SimpleSetting('mode_'+detector.name)
    det_trig = SimpleSetting('trig_'+detector.name)
    det_mode.set('ACCUMULATION')
    det_trig.set('EXTERNAL_TRIGGER_MULTI')
    setup_globals.DEFAULT_CHAIN.set_settings(
        setup_globals.MRTOMO_ext_chain_acc["chain_config"]
    )

    det_def_chain = setup_globals.DEFAULT_CHAIN._settings.get(detector)
    if det_def_chain is None:
        setup_globals.DEFAULT_CHAIN.set_settings(
            setup_globals.MRTOMO_ext_chain["chain_config"]
        )
        raise Exception(
            f"{detector.name} is not configured in default chain for accumulation"
        )


def set_ext_trigger(detector):
    det_mode = SimpleSetting('mode_'+detector.name)
    det_trig = SimpleSetting('trig_'+detector.name)
    det_mode.set('SINGLE')
    det_trig.set('EXTERNAL_TRIGGER_MULTI')
    setup_globals.DEFAULT_CHAIN.set_settings(
        setup_globals.MRTOMO_ext_chain["chain_config"]
    )

    det_def_chain = setup_globals.DEFAULT_CHAIN._settings.get(detector)
    if det_def_chain is None:
        raise Exception(f"{detector.name} is not configured in default chain")


def set_int_accumulation(detector):
    det_mode = SimpleSetting('mode_'+detector.name)
    det_trig = SimpleSetting('trig_'+detector.name)
    det_mode.set('ACCUMULATION')
    det_trig.set('INTERNAL_TRIGGER')
    setup_globals.DEFAULT_CHAIN.set_settings(
        setup_globals.MRTOMO_int_chain_acc["chain_config"]
    )

    det_def_chain = setup_globals.DEFAULT_CHAIN._settings.get(detector)
    if det_def_chain is None:
        setup_globals.DEFAULT_CHAIN.set_settings(
            setup_globals.MRTOMO_int_chain["chain_config"]
        )
        raise Exception(
            f"{detector.name} is not configured in default chain for accumulation"
        )


def set_int_trigger(detector):
    det_mode = SimpleSetting('mode_'+detector.name)
    det_trig = SimpleSetting('trig_'+detector.name)
    det_mode.set('SINGLE')
    det_trig.set('INTERNAL_TRIGGER')
    setup_globals.DEFAULT_CHAIN.set_settings(
        setup_globals.MRTOMO_int_chain["chain_config"]
    )

    det_def_chain = setup_globals.DEFAULT_CHAIN._settings.get(detector)
    if det_def_chain is None:
        raise Exception(f"{detector.name} is not configured in default chain")

#0.012 s sans accumulation
#dark/ref/proj
#roi <200,120>-<2300x2040>
#1200 images
#20 darks
#21 flats

#def multi_half_tomo_insitu(nTomo,sTime):
    #for i in range(nTomo+1):
        #newdataset()
        
        #start = time.time() #start timer
    
        #hrtomo.half_turn_scan() 
        #print('sleeping')
        #time.sleep(sTime)
    
        #end = time.time()   #end timer
        #print('The time of the sequence took:') 
        #print(end-start)    #print time

### NEED TO BE ADAPTED TO NEW TOMO ###

#def zseries_half_turn(zdelta, nbscans, dataset_name='', zstart_pos=None):
    #if dataset_name != '':
        #newdataset(dataset_name)
    #dark_at_start = mrtomo.parameters.dark_images_at_start
    #ref_at_start = mrtomo.parameters.ref_images_at_start
    #dark_at_end = mrtomo.parameters.dark_images_at_end
    #ref_at_end = mrtomo.parameters.ref_images_at_end 
    #if zstart_pos is None:
        #zstart_pos = sz.position
    #lpos = numpy.linspace(zstart_pos,zstart_pos+zdelta*(nbscans-1),nbscans)
    #try:
        #for i in range(nbscans):
            #try:
                #if i == 0:
                    #mrtomo.parameters.dark_images_at_end = False
                    #mrtomo.parameters.ref_images_at_end = False  
                #elif i == 1:
                    #mrtomo.parameters.dark_images_at_start = False
                    #mrtomo.parameters.ref_images_at_start = False    
                #elif i == nbscans - 1:
                    #mrtomo.parameters.dark_images_at_end = dark_at_end
                    #mrtomo.parameters.dark_images_at_end = ref_at_end
                ##newdataset(dataset_name)
                #mv(sz,lpos[i])
                #if i % 2 == 0:
                    #mrtomo.basic_scan(0,180,mrtomo.parameters.tomo_n,mrtomo.parameters.exposure_time)
                #else:
                    #mrtomo.basic_scan(180,0,mrtomo.parameters.tomo_n,mrtomo.parameters.exposure_time)
            #except:
                #raise
                #break
    #except:
        #raise
    #finally:
        #mrtomo.parameters.dark_images_at_start = dark_at_start
        #mrtomo.parameters.ref_images_at_start = ref_at_start
        #mrtomo.parameters.dark_images_at_end = dark_at_end
        #mrtomo.parameters.ref_images_at_end = ref_at_end     
        #mv(sz, zstart_pos)    

#def zseries_half_turn_with_retry(zdelta, nbscans, dataset_name='', zstart_pos=None):
    #if dataset_name != '':
        #newdataset(dataset_name)
    #dark_at_start = mrtomo.parameters.dark_images_at_start
    #ref_at_start = mrtomo.parameters.ref_images_at_start
    #dark_at_end = mrtomo.parameters.dark_images_at_end
    #ref_at_end = mrtomo.parameters.ref_images_at_end 
    #if zstart_pos is None:
        #zstart_pos = sz.position
    #lpos = numpy.linspace(zstart_pos,zstart_pos+zdelta*(nbscans-1),nbscans)
    #try:
        #for i in range(nbscans):
            #try:
                #if i == 0:
                    #mrtomo.parameters.dark_images_at_end = False
                    #mrtomo.parameters.ref_images_at_end = False  
                #elif i == 1:
                    #mrtomo.parameters.dark_images_at_start = False
                    #mrtomo.parameters.ref_images_at_start = False    
                #elif i == nbscans - 1:
                    #mrtomo.parameters.dark_images_at_end = dark_at_end
                    #mrtomo.parameters.dark_images_at_end = ref_at_end
                ##newdataset(dataset_name)
                #mv(sz,lpos[i])
                #if i % 2 == 0:
                    #mrtomo.basic_scan(0,180,mrtomo.parameters.tomo_n,mrtomo.parameters.exposure_time)
                #else:
                    #mrtomo.basic_scan(180,0,mrtomo.parameters.tomo_n,mrtomo.parameters.exposure_time)
            #except KeyboardInterrupt:
                #raise
                #break
            #except:
                #if i % 2 == 0:
                    #mrtomo.basic_scan(0,180,mrtomo.parameters.tomo_n,mrtomo.parameters.exposure_time)
                #else:
                    #mrtomo.basic_scan(180,0,mrtomo.parameters.tomo_n,mrtomo.parameters.exposure_time)
    #except:
        #raise
    #finally:
        #mrtomo.parameters.dark_images_at_start = dark_at_start
        #mrtomo.parameters.ref_images_at_start = ref_at_start
        #mrtomo.parameters.dark_images_at_end = dark_at_end
        #mrtomo.parameters.ref_images_at_end = ref_at_end     
        #mv(sz, zstart_pos)  

def restart_mr_station():
    yrot = setup_globals.yrot
    srot = setup_globals.srot
    sz = setup_globals.sz

    # yrot.controller.initialize()
    # yrot.controller.initialize_hardware()
    # yrot.controller.initialize_axis(yrot)
    # yrot.controller._set_power(yrot, True)
    # yrot.controller.initialize_hardware_axis(yrot)
    yrot.sync_hard()
    yrot.on()
    yrot.sync_hard()
    #while not "READY" in yrot.state:
    if not "READY" in yrot.state:
        #yrot.sync_hard()
        print ("Controller state: " , yrot.controller.state(yrot))
        print ("Bliss state: " , yrot.state())
    yrot.controller.set_velocity(yrot, yrot.velocity * yrot.steps_per_unit)

    # sz.controller.initialize()
    # sz.controller.initialize_hardware()
    # sz.controller.initialize_axis(sz)
    # sz.controller._set_power(sz, True)
    # sz.controller.initialize_hardware_axis(sz)
    sz.sync_hard()
    sz.on()
    sz.sync_hard()
    #while not "READY" in sz.state:
    if not "READY" in sz.state:
        #sz.sync_hard()
        print ("Controller state: " , yrot.controller.state(yrot))
        print ("Bliss state: " , yrot.state())
    sz.controller.set_velocity(sz, sz.velocity * sz.steps_per_unit)

    # srot.controller.initialize()
    # srot.controller.initialize_hardware()
    # srot.controller.initialize_axis(srot)
    # srot.controller._set_power(srot, True)
    # srot.controller.initialize_hardware_axis(srot)
    srot.sync_hard()
    srot.on()
    srot.sync_hard()
    #while not "READY" in srot.state:
    if not "READY" in srot.state:
        #srot.sync_hard()
        print ("Controller state: " , yrot.controller.state(yrot))
        print ("Bliss state: " , yrot.state())
    srot.controller.set_velocity(srot, srot.velocity * srot.steps_per_unit)
    setup_globals.init_mr_srot()
