import pint
import numpy as np
import time


class Pusher():
    def __init__(self):
        self.sx = 0
        self.sy = 0

    def pusher_air_on(self):
        Sam.raw_write(px45, "Dmc2143 sendcommand SB 1")

    def pusher_air_off(self):
        Sam.raw_write(px45, "Dmc2143 sendcommand CB 1") 

    def pushers_move_x(self, rel_pos):
        q = pint.UnitRegistry()
        q.define('pixel = 1 * count = px')
        q.define('fraction = [] = frac')
        q.define('percent = 1e-2 frac = pct')
        ALPHA = 90 * q.deg
        BETA = 90 * q.deg
        GAMMA = 135 * q.deg
        offset = 0 * q.deg
        
        sx45_pos = sx45.position * q.mm
        sy45_pos = sy45.position * q.mm
        rel_pos = rel_pos * q.mm

        # calculate target positions, x/y controlled by offset
        sx45_target = sx45_pos + (rel_pos / np.cos(GAMMA - offset))
        sy45_target = sy45_pos + (rel_pos / np.cos(GAMMA + BETA - offset))

        pusher_air_on()
        mx45.on()
        my45.on()
        umv(sx45, sx45_target.magnitude, sy45, sy45_target.magnitude)
        mx45.off()
        my45.off()
        pusher_air_off()

    def pushers_move_y(self, rel_pos):
        q = pint.UnitRegistry()
        q.define('pixel = 1 * count = px')
        q.define('fraction = [] = frac')
        q.define('percent = 1e-2 frac = pct')
        ALPHA = 90 * q.deg
        BETA = 90 * q.deg
        GAMMA = 135 * q.deg
        offset = ALPHA
        
        sx45_pos = sx45.position * q.mm
        sy45_pos = sy45.position * q.mm
        rel_pos = rel_pos * q.mm

        # calculate target positions, x/y controlled by offset
        sx45_target = sx45_pos + (rel_pos / np.cos(GAMMA - offset))
        sy45_target = sy45_pos + (rel_pos / np.cos(GAMMA + BETA - offset))

        pusher_air_on()
        mx45.on()
        my45.on()
        umv(sx45, sx45_target.magnitude, sy45, sy45_target.magnitude)
        mx45.off()
        my45.off()
        pusher_air_off()

    def pushers_out(self):
        self.sx = sx45.position
        self.sy = sy45.position
        pusher_air_off()
        mx45.off()
        my45.off()
        umv(sx45,0, sy45, 0)
    
    def pushers_in(self):
        pusher_air_on()
        mx45.on()
        my45.on()
        umv(sx45,self.sx, sy45, self.sy)
        mx45.off()
        my45.off()
        pusher_air_off()

#pusher= Pusher()
