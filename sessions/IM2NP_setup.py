from bliss.setup_globals import *
from bliss.common import measurementgroup
from id19.MotorCalibration import *
from tomo.Presets import DefaultChainFastShutterPreset, DefaultChainImageCorrOnOffPreset


# set the default chain configuration
DEFAULT_CHAIN.set_settings(default_acq_chain['chain_config'])

# add shutter control to default scans
shutter_preset = DefaultChainFastShutterPreset(tomopress.shutter)
DEFAULT_CHAIN.add_preset(shutter_preset)
imagecorr_preset = DefaultChainImageCorrOnOffPreset()
DEFAULT_CHAIN.add_preset(imagecorr_preset)

# Open Flint by default
SCAN_DISPLAY.auto=True

# initialise machine information
try:
    machinfo.initialize()
except:
    print("\nNo machine information available!\n")  

# define alignment
from tomo.beamline.ID19.alignment import *

from id19.Tools import *
# define tomo object used for mvct, mvrct umvct, umvrct, cts functions    
init_tomo(tomopress)

# define tomo object used for image correction object
image_corr.set_tomo(tomopress)

load_script("IM2NP.py")

print("")
print("Welcome to your new 'IM2NP' BLISS session !! ")
print("")
print("You can now customize your 'IM2NP' session by changing files:")
print("   * /IM2NP_setup.py ")
print("   * /IM2NP.yml ")
print("   * /scripts/IM2NP.py ")
print("")
