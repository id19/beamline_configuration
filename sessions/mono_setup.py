from bliss.setup_globals import *
from bliss.common import measurementgroup

# set the default chain configuration
DEFAULT_CHAIN.set_settings(default_acq_chain['chain_config'])

# Open Flint by default
SCAN_DISPLAY.auto=True

# initialise machine information
try:
    machinfo.initialize()
    machinfo.check = False
except:
    print("\nNo machine information available!\n")


load_script("mono.py")

print("")
print("Welcome to your new 'mono' BLISS session !! ")
print("")
print("You can now customize your 'mono' session by changing files:")
print("   * /mono_setup.py ")
print("   * /mono.yml ")
print("   * /scripts/mono.py ")
print("")

from id19.mono.id19mono import MonoMode
