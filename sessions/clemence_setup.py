from bliss.setup_globals import *
from tomo.presets import ReferenceMotorPreset
from tomo.standard import *
#from tomo.beamline.BM18.tools import *
from tomo.utils import setup
from tomo.presets import DemoShutterPreset

DEFAULT_CHAIN.set_settings(clemence_chain_acc["chain_config"])

print("")
print("Welcome to your new 'clemence' BLISS session !! ")
print("")
print("You can now customize your 'clemence' session by changing files:")
print("   * /clemence_setup.py ")
print("   * /clemence.yml ")
print("   * /scripts/clemence.py ")
print("")


tomo_config = fake_tomo_config
tomo_config.auto_projection.enabled = False
tomo_config.set_active()

#clean_axis_state(mot_cam,mot_zoom)

tomo_config = fake_tomo_config
full_tomo = fake_full_tomo
#init_tomo(full_tomo)
z_series = fake_z_series
srot = fake_srot
yrot = fake_yrot
sz = fake_sz
sx = fake_sx
sy = fake_sy
att11 = fake_att11
att12 = fake_att12
att13 = fake_att13
att14 = fake_att14
att15 = fake_att15
att21 = fake_att21
att22 = fake_att22
att23 = fake_att23
att24 = fake_att24
att25 = fake_att25
att31 = fake_att31
att32 = fake_att32
att33 = fake_att33
att34 = fake_att34
att35 = fake_att35
psh1 = fake_psh1
psh2 = fake_psh2
psv1 = fake_psv1
psv2 = fake_psv2
ssh1 = fake_ssh1
ssh2 = fake_ssh2
ssv1 = fake_ssv1
ssv2 = fake_ssv2
pcolinux = fake_cam
pco2linux = fake_cam

shutterpreset = DemoShutterPreset()
referencemotorpreset = ReferenceMotorPreset(fake_ref_displ)
flat_scan=full_tomo.get_runner('flat')
flat_scan.add_scan_preset(referencemotorpreset,"motor_preset")

tomo_config.add_scan_preset(['flat', 'return_ref', 'CONTINUOUS', 'tiling','STEP'], shutterpreset)
tomo_config.add_scan_preset(['dark'], shutterpreset)

load_script("clemence.py")
machinfo = FakeMachInfo()
load_script("tomo.py")
