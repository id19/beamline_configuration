from bliss.setup_globals import *
from bliss.common import measurementgroup

# set the default chain configuration
DEFAULT_CHAIN.set_settings(default_acq_chain['chain_config'])

# Open Flint by default
SCAN_DISPLAY.auto=True

# initialise machine information
try:
    machinfo.initialize()
except:
    print("\nNo machine information available!\n")
    

load_script("ID19.py")

from id19.Tools import *   

print("")
print("Welcome to your new 'ID19' BLISS session !! ")
print("")
print("You can now customize your 'ID19' session by changing files:")
print("   * /ID19_setup.py ")
print("   * /ID19.yml ")
print("   * /scripts/ID19.py ")
print("")


def clemence_pulse(width=100,delay=10):
    #width and delay in ms
    opiom2_tpg = get_config().get("opiom2_tpg")
    opiom2_tpg.opiom.comm_ack("CNT 2 RESET")
    cmd = "CNT 2 CLK8 RISE D32 PULSE "+str(int(width*8000))+" "+str(int(delay*8000))+" "+str(1)
    opiom2_tpg.opiom.comm_ack(cmd)
    opiom2_tpg.opiom.comm_ack("CNT 2 START")


def marta_pulse(width=100,delay=10):
    opiom2_tpg = get_config().get("opiom2_tpg")
    opiom2_tpg.opiom.comm_ack("CNT 2 RESET")
    cmd = "CNT 2 CLK8 RISE D32 PULSE "+str(int(width*8000))+" "+str(int(delay*8000))+" "+str(1)
    opiom2_tpg.opiom.comm_ack(cmd)
    opiom2_tpg.opiom.comm_ack("CNT 2 START")
