//****************************************************************
// SWEEP : 
//****************************************************************
// Description:
//   The program generates NPULSES gates on OutB starting from POSSTART.
//   Gate width is given by POSDELTA. 
//   Second gate is generated exactly at end position of previous one.
//   Data are stored at rising and falling edge of each gate. By default
//   timer and MOTCHAN position are stored. Additionnal data can
//   be stored using STOREMASK.
//   Scanning can be done in both direction. Direction is given by
//   sign of POSDELTA
//****************************************************************
//
// OutA     |            
//          |_________  
// OutB     |        | 
//          |        |
// Pos  --------------------------------------->
//          ^        
//       POSSTART     
// 
// OutA              |            
//                   |_________  
// OutB              |        | 
//                   |        |
// Pos  --------------------------------------->
//                   |<------>|
//                    POSDELTA
//
//      ... repeated NPULSES times ...
//
// Buffer   *        *        
//                   *        *
//                            *       * ...
//****************************************************************

// --- INPUT VARIABLES
SIGNED POSSTART
SIGNED POSDELTA
UNSIGNED MOTERR
UNSIGNED NPULSES
UNSIGNED UNDERSHOOT

// --- INTERNAL VARIABLES
SIGNED SCANDIR
SIGNED LAST_TG
SIGNED NEXT_TG
UNSIGNED CUMERR
SIGNED MOTCORR
UNSIGNED NBCORR
SIGNED BACKDELTA

// --- OUTPUT VARIABLES
UNSIGNED NPOINTS

// --- PROGRAM ALIASES
ALIAS MOTCHAN = $MOTOR_CHANNEL$

SUB STORE_INIT
	STORELIST TIMER MOTCHAN
	EMEM 0 AT 0
ENDSUB
	

// --- init event source
SUB SET_NORMAL_DIR
	DEFEVENT MOTCHAN
	IF (SCANDIR > 0) THEN
		EVSOURCE MOTCHAN UP
	ELSE
		EVSOURCE MOTCHAN DOWN
	ENDIF
ENDSUB

// --- inverse event direction
SUB SET_INVERT_DIR
	IF (SCANDIR > 0) THEN
		EVSOURCE MOTCHAN DOWN
	ELSE
		EVSOURCE MOTCHAN UP
	ENDIF
ENDSUB

// --- increment target position
// --- and correct error if needed  
SUB INC_TARGET
	NEXT_TG = LAST_TG + POSDELTA
	
	CUMERR += MOTERR
	IF ((CUMERR & 0x80000000) != 0) THEN
		CUMERR &= 0x7FFFFFFF
		NEXT_TG += MOTCORR
		NBCORR += 1
	ENDIF
	
	LAST_TG = NEXT_TG
	@MOTCHAN= NEXT_TG
ENDSUB

// --- main program loop
PROG SWEEP

	// --- reset Trigger Out B signal
	BTRIG 0

	// --- reset timer
	CTSTOP TIMER
	CTRESET TIMER
	TIMER = 0
	CTSTART ONEVENT TIMER
	
	// --- init store values	
	GOSUB STORE_INIT

	// --- init variables
	NPOINTS = 0
	LAST_TG = POSSTART
	NEXT_TG = POSSTART
	
	IF (POSDELTA < 0) THEN
		SCANDIR= -1
		MOTCORR= -1
		BACKDELTA= UNDERSHOOT
	ELSE
		SCANDIR= 1
		MOTCORR= 1
		BACKDELTA= -UNDERSHOOT
	ENDIF

	CUMERR= 0
	NBCORR= 0

	// --- init event and first target		
	GOSUB SET_NORMAL_DIR
	@MOTCHAN = NEXT_TG
	
	// --- main loop
	WHILE (NPOINTS < NPULSES) DO
		AT DEFEVENT DO STORE BTRIG ATRIG
		GOSUB INC_TARGET
		AT DEFEVENT DO STORE BTRIG
		IF (NPOINTS < NPULSES-1) THEN
			GOSUB SET_INVERT_DIR
			@MOTCHAN= LAST_TG + BACKDELTA
			AT DEFEVENT DO NOTHING
			GOSUB SET_NORMAL_DIR
			@MOTCHAN= LAST_TG
		ENDIF
		
		NPOINTS += 1
	ENDWHILE
	
	EXIT NPOINTS
ENDPROG

// --- cleanup function
PROG SWEEPCLEAN
	CTSTOP TIMER
	TIMER = 0
	
	BTRIG 0
ENDPROG
	
	
	
